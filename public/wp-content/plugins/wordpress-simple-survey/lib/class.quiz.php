<?php
/* WordPress Simple Survey - Quiz Class*/
class wpss_quiz{


  /* Load Quiz with Questions and Answers  or All Quizes */
  public function __construct($id = 'ALL') { 

    if(is_numeric($id)) $this->load_quiz($id);
  }
 

 
  /* Load Quiz */
  public function load_quiz($id){
    global $wpdb;
    $q = $wpdb->get_results('SELECT * FROM '.WPSS_QUIZZES_DB.' WHERE id="'.$id.'" LIMIT 1', ARRAY_A);
    $quiz = $q[0];
    if(empty($quiz)) return '';
    return $quiz;
  }

  


}

?>
