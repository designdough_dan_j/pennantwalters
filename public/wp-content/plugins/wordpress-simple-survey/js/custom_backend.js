(function($) { 
  $(function() {

    /* Tooltip for admin backend examples */
    $(".wpss_info").tooltip({position: "center right", opacity: 1.0});

    /* Sliding results for admin view of submissions */
    $("#quiz_summary_holder").accordion();
    
  });
})(jQuery);
