# Translation of Plugins - Public Post Preview - Stable (latest release) in English (UK)
# This file is distributed under the same license as the Plugins - Public Post Preview - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-03-14 10:54:18+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: en_GB\n"
"Project-Id-Version: Plugins - Public Post Preview - Stable (latest release)\n"

#. Plugin URI of the plugin/theme
msgid "https://dominikschilling.de/wp-plugins/public-post-preview/en/"
msgstr "https://dominikschilling.de/wp-plugins/public-post-preview/en/"

#. Author URI of the plugin/theme
msgid "http://wphelper.de/"
msgstr "http://wphelper.de/"

#. Author of the plugin/theme
msgid "Dominik Schilling"
msgstr "Dominik Schilling"

#. Description of the plugin/theme
msgid "Enables you to give a link to anonymous users for public preview of any post type before it is published."
msgstr "Enables you to give a link to anonymous users for public preview of any post type before it is published."

#. Plugin Name of the plugin/theme
msgid "Public Post Preview"
msgstr "Public Post Preview"

#: public-post-preview.php:452
msgid "No Public Preview available!"
msgstr "No Public Preview available!"

#: public-post-preview.php:448
msgid "The link has been expired!"
msgstr "The link has been expired!"

#: public-post-preview.php:201
msgid "(Copy and share this link.)"
msgstr "(Copy and share this link.)"

#: public-post-preview.php:196
msgid "Enable public preview"
msgstr "Enable public preview"

#: public-post-preview.php:123
msgid "Public Preview"
msgstr "Public Preview"

#: public-post-preview.php:107
msgid "Disabled!"
msgstr "Disabled!"

#: public-post-preview.php:106
msgid "Enabled!"
msgstr "Enabled!"