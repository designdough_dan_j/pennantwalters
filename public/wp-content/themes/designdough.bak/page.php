<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <?php
    while ( have_posts() ) : the_post();
    
        global $flex_row;
        $flex_row = 1;

        if ( have_rows('flex') ){
            get_template_part( 'flex' );
        } else { ?>
            <div class="flex-row flex-row-1">
                <div class="wrap">
                    <div class="content"> 
                        <?php echo the_content(); ?>    
                    </div>
                </div>
            </article>
        <?php }

    endwhile; // End of the loop.
    ?>

  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer();