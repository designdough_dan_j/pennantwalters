<?php

header('Content-Type: application/json');

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$args = array(
  'offset' => 1,
  'posts_per_page' => -1,
  'post_type' => 'portfolio-piece'
);

$posts = get_posts($args);

$posts_array = array();

foreach( $posts as $post ){
  $cats = array();
  $tags = array();
  
  foreach (get_the_terms($post->ID, 'portfolio-category') as $cat){
    $cats[] = $cat->name;
  }
  
  foreach (get_the_terms($post->ID, 'portfolio-tag') as $tag){
    $tags[] = $tag->name;
  }
    
  $posts_array[] = array(
    'post_title' => $post->post_title,
    'link' =>  get_the_permalink( $post->ID ),
    'img' => get_the_post_thumbnail_url( $post->ID ),
    'excerpt' => $post->post_excerpt,
    'project' => get_field('project', $post->ID),
    'cats' => $cats,
    'tags' => $tags
  );
}

print json_encode($posts_array, true);