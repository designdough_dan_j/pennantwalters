<?php
/**
* The flexible content of the theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package WordPress
* @subpackage designdough
* @since 1.0
* @version 1.0
*/

$row = get_row_index();

?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<div <?php the_flex_classes($row); ?>>
	<div class="wrap">
		<?php sideways_header($row, get_sub_field('sideways_header')); ?>

		<h2><?php the_sub_field('title'); ?></h2>

		<div class="content form-wrap">
			<div class="form-response">
				<div class="inner"></div>
			</div>

			<form id="contact_form_<?php the_row_index(); ?>" method="post" action="<?php echo get_template_directory_uri(); ?>/scripts/send-email.php">
				<div class="cols count-3">
					<div class="col">
						<div class="form-group">
							<input required type="text" placeholder="Full Name" id="full_name" name="full_name" />
							<label class="hidden">Full Name</label>
						</div>

						<div class="form-group">
							<input type="text" placeholder="Company/Organisation" id="company_name" name="company_name" />
							<label class="hidden">Company/Organisation Name</label>
						</div>

						<div class="form-group">
							<input required type="email" placeholder="Email Address" id="email_address" name="email_address" />
							<label class="hidden">Email Address</label>
						</div>

						<div class="form-group">
							<input required type="tel" placeholder="Contact No." id="contact_no" name="contact_no" />
							<label class="hidden">Contact No.</label>
						</div>

						<div class="g-recaptcha" data-sitekey="6LfuIUgUAAAAAIiIpmxLVI-NLZ21FyaaqnW3VRkd"></div>
					</div>

					<div class="col">
						<div class="form-group full-height">
							<textarea class="full-height" required placeholder="Enquiry" id="enquiry" name="enquiry"></textarea>

							<label class="hidden">Enquiry</label>
						</div>
					</div>

					<div class="col">
						<button class="btn">Send Message</button>
					</div>
				</div>

				<input type="text" name="name" style="width: 0; height: 0; opacity: 0;" />
			</form>
		</div>
	</div>
</div>
<script>
(function ($, root, undefined) {
	$(function(){
		$('#contact_form_<?php echo get_row_index(); ?>').validate({
			errorPlacement: function(error, element) {
				error.appendTo( element.parent('.form-group') );
			},
			messages: {
				full_name: "Please specify your name",
				email_address: {
					required: "We need your email address to contact you",
					email: "Pleae enter a valid email address"
				},
				enquiry: "Enter your message"
			},
			submitHandler: function(form) {
				event.preventDefault();

				if ( $(form).find('input[name="name"]').val() != '') {
					$('.form-response').fadeIn(250).find('.inner').html('Spam detected');
					return;
				}

				var wrap = $(form).parent('.form-wrap');

				$.ajax({
					type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
					url: $(form).attr('action'), // the url where we want to POST
					data: $(form).serialize(), // our data object
				})
				.done(function(data) {
					$('.form-response').fadeIn(250).find('.inner').html(data);

					setTimeout(function(){
						$('.form-response').fadeOut(250);
						$(form)[0].reset();
					}, 10000);
				});
			}
		});
	});
})(jQuery, this);
</script>
