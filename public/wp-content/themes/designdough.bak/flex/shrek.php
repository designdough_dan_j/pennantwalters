<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();

?>
<div <?php the_flex_classes($row); ?>>
	<div class="wrap">
		<?php sideways_header($row, get_sub_field('sideways_header')); ?>

		<div class="shrek-info">

			<h2 class="flex-title"><?php the_sub_field('title'); ?></h2>

			<div class="content">
				<?php the_sub_field('content'); ?>
			</div>
		</div>

		<div class="shrek-links">
			<?php $shrekLinks = get_sub_field('shrek_links');
			foreach ($shrekLinks as $link) {?>
			<a class="shrek-link" href="<?php echo $link['shrek_link'];?>">
				<?php echo $link['shrek_button'];?>
				<span class="icon-arrow-right"></span>
			</a>
			<?php } ?>
		</div>
	</div>
</div>
