<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();
$offices = get_field('offices', 39);
?>
<div <?php the_flex_classes($row); ?>>
  <div class="wrap">
    <?php sideways_header($row, get_sub_field('sideways_header')); ?>
    <div class="content">
      <div class="map-wrap">
        <div id="map" class="map"></div>
        
        <?php if( get_sub_field('map_select') === "radio" ): ?>
        <div id="office_list" class="office-list no-bullets">
          <ul>
            <?php

            // check if the repeater field has rows of data
            if( have_rows('offices', 39) ):
       
              $marker = 0;

              // loop through the rows of data
              while ( have_rows('offices', 39) ) : the_row();
            ?>
            <li>
              <label>
                <input name="office_location" type="radio" onClick="openInfoWindow(<?php echo $marker++; ?>);" />
                <h3><?php the_sub_field('office_name'); ?></h3>
              </label>
            </li>
            <?php

              endwhile;

            endif;

            ?>
          </ul>
        </div>
        <?php else: ?>
        <div id="office_list" class="select-filter">
          <label>Map Marker:</label>
          <div class="select icon-caret">
            <select name="office_location" onChange="openInfoWindow(this.value);">
              <?php

              // check if the repeater field has rows of data
              if( have_rows('offices', 39) ):

                $marker = 0;

                // loop through the rows of data
                while ( have_rows('offices', 39) ) : the_row();
              ?>
              <option value="<?php echo $marker++; ?>"><?php the_sub_field('office_name'); ?></option>
              <?php

                endwhile;

              endif;

              ?>
            </select>
          </div>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<script>
  var map;
  var markers = [];

  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: <?php echo $offices[0]['office_latitude']; ?>
        , lng: <?php echo $offices[0]['office_longitude']; ?>
      }
      , zoom: 8
      , scrollwheel: false
      , styles: [
        {
          "elementType": "geometry"
          , "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        }
        , {
          "elementType": "labels.icon"
          , "stylers": [
            {
              "visibility": "off"
            }
          ]
        }
        , {
          "elementType": "labels.text.fill"
          , "stylers": [
            {
              "color": "#616161"
            }
          ]
        }
        , {
          "elementType": "labels.text.stroke"
          , "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        }
        , {
          "featureType": "administrative.land_parcel"
          , "elementType": "labels.text.fill"
          , "stylers": [
            {
              "color": "#bdbdbd"
            }
          ]
        }
        , {
          "featureType": "poi"
          , "elementType": "geometry"
          , "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        }
        , {
          "featureType": "poi"
          , "elementType": "labels.text.fill"
          , "stylers": [
            {
              "color": "#757575"
            }
          ]
        }
        , {
          "featureType": "poi.park"
          , "elementType": "geometry"
          , "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        }
        , {
          "featureType": "poi.park"
          , "elementType": "labels.text.fill"
          , "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        }
        , {
          "featureType": "road"
          , "elementType": "geometry"
          , "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        }
        , {
          "featureType": "road.arterial"
          , "elementType": "labels.text.fill"
          , "stylers": [
            {
              "color": "#757575"
            }
          ]
        }
        , {
          "featureType": "road.highway"
          , "elementType": "geometry"
          , "stylers": [
            {
              "color": "#dadada"
            }
          ]
        }
        , {
          "featureType": "road.highway"
          , "elementType": "labels.text.fill"
          , "stylers": [
            {
              "color": "#616161"
            }
          ]
        }
        , {
          "featureType": "road.local"
          , "elementType": "labels.text.fill"
          , "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        }
        , {
          "featureType": "transit.line"
          , "elementType": "geometry"
          , "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        }
        , {
          "featureType": "transit.station"
          , "elementType": "geometry"
          , "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        }
        , {
          "featureType": "water"
          , "elementType": "geometry"
          , "stylers": [
            {
              "color": "#c9c9c9"
            }
          ]
        }
        , {
          "featureType": "water"
          , "elementType": "labels.text.fill"
          , "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        }
      ]
    });
    setMarkers(map);
  }
  // Data for markers
  var offices = [
    <?php

    // check if the repeater field has rows of data
    if( have_rows('offices', 39) ):

      // loop through the rows of data
      while ( have_rows('offices', 39) ) : the_row();
    ?>
    ['<?php the_sub_field('office_name'); ?>', '<?php echo preg_replace("/[\n\r]/","", get_sub_field('office_address')); ?>','<?php the_sub_field('office_tel') ?>','<?php the_sub_field('office_email') ?>', <?php the_sub_field('office_latitude'); ?>, <?php the_sub_field('office_longitude'); ?>], 
    <?php

      endwhile;

    else :

      // no rows found

    endif;

    ?>
  ];

  function setMarkers(map) {
    // Adds markers to the map.
    // Marker sizes are expressed as a Size of X,Y where the origin of the image
    // (0,0) is located in the top left of the image.
    for (var i = 0; i < offices.length; i++) {
      var office = offices[i];
      var marker = new google.maps.Marker({
        position: {
          lat: office[4], lng: office[5]
        },
        map: map,
        icon: {
          url: '<?php echo get_template_directory_uri();?>/img/marker.png',
          size: new google.maps.Size(34, 45)
        },
        title: office[0],
      });
      
      var marker, i;
      var infowindow = new google.maps.InfoWindow({
        content: '<div class="map-office-info"><h3>' + office[0] + '</h3>' + office[1] + '<p><span class="highlight">T</span> <a href="tel:+44' + office[2] + '">0' + office[2] + '</a><br/><span class="highlight">E</span> <a href="mailto:' + office[3] + '">' + office[3] + '</a></p></div>'
      });
      //creates an infowindow 'key' in the marker.
      marker.infowindow = infowindow;
      //finally call the explicit infowindow object
      marker.addListener('click', function (marker, i) {
        //map.setZoom(14);
        map.setCenter(this.getPosition());
        return this.infowindow.open(map, this);
      });
      markers.push(marker);
    }
  }

  function openInfoWindow(id) {
    google.maps.event.trigger(markers[id], 'click');
  }
  
  (function ($, root, undefined) {
    $(function(){
      
      if (typeof openInfoWindow == 'function') { 
        openInfoWindow(0);
      }
    
    });
  })(jQuery, this);
  
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE01ETxTU2ruQL2gM2U4ztVrLxfJUcrOA&callback=initMap" ></script>