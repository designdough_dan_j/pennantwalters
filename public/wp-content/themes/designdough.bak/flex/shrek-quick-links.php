<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();

if ( ! post_password_required() ) {
	?>
	<div <?php the_flex_classes($row); ?>>

		<?php
		$banner = get_sub_field('banner');
		if ( $banner )
		{ ?>
		<div class="wrap">
			<div class="shrek-header" style="background-image: url(<?php echo $banner['url']?>)"></div>
		</div>
		<?php } ?>

		<div class="quick-links">
			<div class="wrap">
				<?php sideways_header($row, get_sub_field('sideways_header')); ?>

				<div class="shrek-info">

					<h2 class="flex-title"><?php the_sub_field('title'); ?></h2>

					<div class="content">
						<?php the_sub_field('content'); ?>
					</div>
				</div>

				<div class="shrek-links">
					<?php $shrekLinks = get_sub_field('shrek-quick-links');
					foreach ($shrekLinks as $link)
					{ ?>
					<a class="shrek-link quick-link" href="<?php echo $link['shrek_link'];?>">
						<div class="icon" style="background-image: url(<?php echo $link['shrek_icon']['url']; ?>)"></div>

						<div class="link">
							<?php echo $link['shrek_button']; ?>
						</div>

						<span class="icon-arrow-right"></span>
					</a>
					<?php
					} ?>
				</div>
			</div>
		</div>
	</div>

	<script>
		(function ($, root, undefined) {
			$(function () {
				resizeLinks();

				$(window).resize( function () {
					resizeLinks();
				});

				function resizeLinks() {
					var columns;

					if ( $(window).width() >= 1300 ) {
						columns = 3;
					} else if ( $(window).width() >= 599 && $(window).width() < 1300) {
						columns = 2;
					} else {
						columns = 1;
					}

					var height = 0;
					var itteration = 0;

					$('.shrek-link.quick-link').height('auto').each( function () {
						itteration++;
						$(this).addClass('counted');

						if ( $(this).height() > height ) {
							height = $(this).height();
							console.log($(this).height() +' - '+ height);
						}

						if ( itteration == columns ) {
							$('.shrek-link.quick-link.counted').height(height).removeClass('counted');

							height = 0;
							itteration = 0;
						}
					});
				}
			});
		})(jQuery, this);
	</script>
<?php } else { ?>
	<div <?php the_flex_classes($row); ?>>
		<div class="wrap">
			<?php sideways_header($row, get_sub_field('sideways_header')); ?>

			<h2 class="flex-title"><?php the_sub_field('title'); ?></h2>

		    <div class="content">
				<p>This page is restricted, please enter the password.</p>

				<form action="<?php echo esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) ?>" method="post">
					<input name="post_password" type="password" size="20" maxlength="20" />
					<br>
					<button type="submit" name="Submit" class="button">Login</button>
				</form>
			<div>
		</div>
	</div>
<?php } ?>
