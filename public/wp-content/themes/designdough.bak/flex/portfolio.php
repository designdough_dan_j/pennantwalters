<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();

?>
<div <?php the_flex_classes($row); ?>>
  <div class="wrap">
    <?php sideways_header($row, 'Recent Projects'); ?>
    <div class="content">
      <h2 class="flex-title"><?php the_sub_field('title'); ?></h2>
      <div id="project_slider" class="slick-slider">
      <?php

      global $post;
      $args = array( 'posts_per_page' => -1, 'posts_per_page' => 6, 'post_type' => 'portfolio-piece' );

      $posts = get_posts( $args ); $x = 1;
      foreach ( $posts as $post ) : setup_postdata( $post ); ?>
        <div class="slick-slide">
            <a class="portfolio-item" href="<?php the_permalink(); ?>">
              <div class="item-info">
                <h3><?php the_title(); ?></h3>
                <p><?php the_field('project'); ?></p>
                <span class="icon-arrow-right"></span>
              </div>
              <span class="thumb" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></span>
          </a>
        </div>
      <?php $x++; endforeach; 
      wp_reset_postdata();?>
      </div>
    </div>
  </div>
</div>
<script>
(function ($, root, undefined) {
	
  $(function () {

    $('#project_slider').slick({
      slidesToShow: 5,
      responsive: [
        {
          breakpoint: 2560,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            adaptiveHeight: true,
          }
        }
      ]
    });
  });
	
})(jQuery, this);
</script>