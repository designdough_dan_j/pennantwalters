<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

global $flex_row;
global $social;

?>
      <footer id="footer">
        <div class="wrap">
          <?php $offices = get_field('offices', 39); ?>
          <div class="main-office">
            <a class="logo" href="<?php echo home_url(); ?>"><?php echo bloginfo('sitename'); ?></a>
            <p><a href="tel:+44:<?php echo $offices[0]['office_tel']; ?>"><span class="highlight">T</span> &nbsp; 0<?php echo $offices[0]['office_tel']; ?></a></p>
            <p><a href="mailto:<?php echo $offices[0]['office_email']; ?>"><span class="highlight">E</span> &nbsp; <?php echo $offices[0]['office_email']; ?></a></p>
          </div>
          <?php echo do_shortcode('[offices show_tel="0" show_email="0"]'); ?>
        </div>
        <hr/>
        <div class="wrap">
          <div class="social-links no-bullets">
            <ul class="clearfix">
              <?php foreach( $social as $key ): ?>
              <li><a title="<?php echo $key; ?>" class="hide-text icon-<?php echo sanitize_title($key); ?>" target="_blank" href="<?php the_field(sanitize_title($key), 39); ?>"><?php echo $key; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
          <div class="count-5">
            <div class="grid">
              <div class="col col-1">
                <div class="nav no-bullets">
                  <ul>
                  <?php $nav_1 = array(31, 33, 39, 202); ?>
                  <?php foreach ($nav_1 as $key => $value): ?>
                    <?php if( $key === 2 ): ?>
                    <li><a href="<?php echo get_post_type_archive_link( 'portfolio-piece' ); ?>">Portfolio</a></li>
                    <?php endif; ?>
                    <li><a href="<?php echo get_the_permalink($value); ?>"><?php echo get_the_title($value); ?></a></li>
                  <?php endforeach; ?>
				  <li><a href="<?php echo get_post_type_archive_link('plant'); ?>">Plant Fleet</a></li>
                  </ul>
                </div>
              </div>
              <div class="col col-2">
                <div class="nav no-bullets">
                  <ul>
                    <li>
                      <a href="<?php echo get_the_permalink(35); ?>">About</a>
                      <ul class="sub-menu">
                        <li><a href="<?php echo get_the_permalink(35); ?>/#thewaltersway">The Walters Way</a></li>
                        <li><a href="<?php echo get_the_permalink(35); ?>/#history">History</a></li>
                        <li><a href="<?php echo get_the_permalink(35); ?>/#team">Team</a></li>
                        <li><a href="<?php echo get_the_permalink(35); ?>/#compliance">Compliance</a></li>
                        <li><a href="<?php echo get_the_permalink(35); ?>/#community">Community</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col col-3">
                <div class="nav no-bullets">
                  <ul>
                      <?php

                      // check if the repeater field has rows of data
                      if( have_rows('services_repeater', 37) ):

                          // loop through the rows of data
                          while ( have_rows('services_repeater', 37) ) : the_row();
                      ?>
                      <li><a href="<?php echo get_the_permalink(37); ?>#<?php echo sanitize_key(get_sub_field('name')); ?>"><?php the_sub_field('name'); ?></a></li>
                      <?php

                          endwhile;

                      endif;

                      ?>
                  </ul>
                </div>
              </div>
              <div class="col col-4">
                <div class="nav no-bullets">
                  <ul>
                      <?php

                      // check if the repeater field has rows of data
                      if( have_rows('services_repeater', 206) ):

                          // loop through the rows of data
                          while ( have_rows('services_repeater', 206) ) : the_row();
                      ?>
                      <li><a href="<?php echo get_the_permalink(206); ?>#<?php echo sanitize_key(get_sub_field('name')); ?>"><?php the_sub_field('name'); ?></a></li>
                      <?php

                          endwhile;

                      endif;

                      ?>
                  </ul>
                </div>
              </div>
              <div class="col col-5">
                <div class="nav no-bullets">
                  <ul>
                      <?php

                      // check if the repeater field has rows of data
                      if( have_rows('services_repeater', 209) ):

                          // loop through the rows of data
                          while ( have_rows('services_repeater', 209) ) : the_row();
                      ?>
                      <li><a href="<?php echo get_the_permalink(209); ?>#<?php echo sanitize_key(get_sub_field('name')); ?>"><?php the_sub_field('name'); ?></a></li>
                      <?php

                          endwhile;

                      endif;

                      ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="copyright">
          <div class="wrap">
            &copy; <?php echo bloginfo('name'); ?> <?php echo date('Y'); ?> &nbsp; | &nbsp; All Rights Reserved
            <div class="nav">
              <a href="<?php echo get_the_permalink(786); ?>"><?php echo get_the_title(786); ?></a> &nbsp; | &nbsp; <a href="<?php echo get_the_permalink(788); ?>"><?php echo get_the_title(788); ?></a><!-- &nbsp; | &nbsp; <a>Design By Designdough</a> -->
            </div>
          </div>
        </div>
      </footer>
    </div>
    </div>
    <!-- /Site Wrap -->
    <?php wp_footer(); ?>
  </body>
</html>
