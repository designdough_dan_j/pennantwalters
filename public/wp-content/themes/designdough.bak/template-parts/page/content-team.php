<div id="team_extra" class="extra-tab-content">
  <div class="grid team clearfix">
    <?php

    // check if the repeater field has rows of data
    if( have_rows('team') ): $x = 1;

      // loop through the rows of data
      while ( have_rows('team') ) : the_row(); $img = get_sub_field('img');
    
    if( $img ){
      $photo = $img['url'];
    } else {
      $photo = get_template_directory_uri().'/img/person-placeholder-'.get_sub_field('placeholder').'.jpg';
    }
    
    ?>
    <div class="col col-<?php echo $x; ?>" data-category="<?php echo sanitize_title(get_sub_field('category')) ?>">
      <div class="team-member-wrap">
        <img src="<?php echo $photo; ?>" alt="<?php the_sub_field('name'); ?> - <?php the_sub_field('role'); ?>" />
        <div class="team-member-bio">
          <div class="inner">
            <h3 class="team-member-name"><?php the_sub_field('name'); ?></h3>
            <p class="team-member-role"><?php the_sub_field('role'); ?></p>
            <?php if( get_sub_field('email') ): ?>
            <a class="team-member-email" href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <?php
      $x++;
      endwhile;

    endif;

    ?>
  </div>
</div>