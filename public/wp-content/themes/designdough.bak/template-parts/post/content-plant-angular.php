<div class="col col-{{ $index + 1 }}" data-cat="{{ x.cat }}" ng-repeat="x in posts | filter: posts_filter | limitTo: postsLimit">
	<div class="plant-item" ng-click="viewItem(x.post_id)">
		<p class="plant-header">
			{{ x.cat.name }}
			<span>Fleet: <b>{{ x.fields.count }}</b></span>
		</p>

		<div class="plant-thumb" style="background-image:url('{{ x.img }}');"></div>

		<div class="plant-stats">
			<h3>{{ x.post_title }}</h3>
			<hr>

			{{ x.description }}

			<a href="{{ x.fields.link }}" class="icon-arrow-right"></a>
		</div>
	</div>
</div>
