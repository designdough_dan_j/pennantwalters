<div class="col">
  <a class="portfolio-item" href="<?php the_permalink(); ?>">
    <div class="item-info">
      <h3><?php the_title(); ?></h3>
      <p><?php the_field('project'); ?></p>
      <span class="icon-arrow-right"></span>
    </div>
    <span class="thumb" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></span>
</a>
</div>