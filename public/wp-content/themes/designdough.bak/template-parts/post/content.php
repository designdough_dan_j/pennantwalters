<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="<?php if( !is_home() || !is_archive() ): ?>col<?php else: ?>slick-slide<?php endif; ?>">
  <div class="news-item">
    <div class="tags no-bullets">
      <ul class="clearfix">
        <?php foreach( get_the_tags() as $tag ): ?>
        <li><a href="<?php echo get_permalink( get_option('page_for_posts' ) ); ?>&tag=<?php echo $tag->name; ?>"><?php echo $tag->name; ?></a></li>
        <?php endforeach; ?>
      </ul>
    </div>
    <a class="thumb" href="<?php the_permalink(); ?>" style="background-image:url(<?php the_post_thumbnail_url(); ?>)"></a>
    <h3><?php the_title(); ?></h3>
    <?php the_excerpt(); ?>
    <a href="<?php the_permalink(); ?>" class="hide-text icon-arrow-right" tabindex="0"><span class="screen-reader-text">Read More</span></a>
  </div>
</div>