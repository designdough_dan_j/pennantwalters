<?php
/**
 *
 * Template Name: Services
 *
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <?php $flex_row = 1; ?>
    <div class="flex-row flex-row-<?php echo $flex_row; ?> flex-hero">
      <div class="wrap">
        <?php sideways_header($flex_row, get_the_title()); ?>
        <div class="content">
          <div id="hero_vid" class="hero-video" style="display:block;">
            <img id="hero_thumb" src="" style="display: none;">
            <div class="replay-overlay"><a class="replay-btn"><i class="icon-replay" aria-hidden="true"></i>Replay</a></div>
            <div class="video-wrap">
              <video autoplay>
                <source src="<?php echo get_field('services_repeater')[0]['video'] ?>" type="video/mp4">
                </video>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php $flex_row++; ?>
    <div class="flex-row flex-row-<?php echo $flex_row; ?>">
      <div class="wrap">
        <div class="content clearfix">
         <?php sideways_header($flex_row, get_the_title(), 1); ?>
          <aside id="content_left">
            <h1><?php the_title(); ?></h1>
            <?php

            // check if the repeater field has rows of data
            if( have_rows('services_repeater') ): $key = 0;

                // loop through the rows of data
                while ( have_rows('services_repeater') ) : the_row();
            ?>
            <div id="<?php echo sanitize_key(get_sub_field('name')); ?>_pull_quote" class="pull-quote" style="<?php if($key === 0): ?>display:block;<?php endif; ?>">
              <?php the_sub_field('pull_quote'); ?>
            </div>
            <?php

            $key++;

                endwhile;

            endif;

            ?>
          </aside>
          <article id="content_right">
            <div class="nav nav-tabs count-<?php echo count(get_field('services_repeater')); ?> no-bullets">
              <hr/>
              <ul class="clearfix">
                <?php

                // check if the repeater field has rows of data
                if( have_rows('services_repeater') ): $key = 0;

                    // loop through the rows of data
                    while ( have_rows('services_repeater') ) : the_row();

                      $classes = array();

                      if( get_sub_field('video') ){
                        $classes[] = "has-video";
                      }

                      if($key === 0){
                        $classes[] = "active";
                      }
                ?>
                <li>
                  <a id="<?php echo sanitize_key(get_sub_field('name')); ?>_btn" class="<?php echo implode(" ", $classes); ?>" href="#<?php echo sanitize_key(get_sub_field('name')); ?>" <?php if(get_sub_field('video')): ?>data-video="<?php echo get_sub_field('video'); ?>"<?php endif; ?> <?php if(get_sub_field('img')): ?>data-img="<?php echo get_sub_field('img')['url']; ?>"<?php endif; ?>><span class="ball" title="<?php the_sub_field('name'); ?>"></span><span class="title"><?php the_sub_field('name'); ?></span></a>
                </li>
                <?php

                    $key++;
                    endwhile;

                endif;

                ?>
              </ul>
            </div>

            <div class="tab-intros">
              <?php

              // check if the repeater field has rows of data
              if( have_rows('services_repeater') ):

                  // loop through the rows of data
                  while ( have_rows('services_repeater') ) : the_row();
              ?>
              <div id="<?php echo sanitize_key(get_sub_field('name')); ?>_intro" class="tab-intro" style="<?php if( get_row_index() === 1 ): ?>display:block;<?php endif; ?>">
                <h3><?php the_field(sanitize_key(get_sub_field('name')).'_title'); ?></h3>
                <?php // the_field(sanitize_key(get_sub_field('name')).'_content') ?>
              </div>
              <?php
                  endwhile;

              endif;

              ?>
            </div>
            <div class="tab-content-area">
              <?php

              // check if the repeater field has rows of data
              if( have_rows('services_repeater') ):

                  // loop through the rows of data
                  while ( have_rows('services_repeater') ) : the_row();
              ?>
              <div id="<?php echo sanitize_key(get_sub_field('name')); ?>" class="tab-content" style="<?php if( get_row_index() === 1 ): ?>display:block;<?php endif; ?>">
                <?php the_sub_field('content'); ?>
              </div>
              <?php

                  endwhile;

              endif;

              ?>
            </div>
          </article>
        </div>
        <div class="content clearfix">
          <!-- Extra Tab Content -->
          <?php

          // check if the repeater field has rows of data
          if( have_rows('team') ){ $x = 1;
            include(locate_template('template-parts/page/content-team.php'));
          }

          // check if the repeater field has rows of data
          if( have_rows('timeline') ){ $x = 1;
            include(locate_template('template-parts/page/content-timeline.php'));
          }

          ?>
          <!-- /Extra Tab Content -->
        </div>
      </div>
    </div>
    <?php $flex_row++; ?>

    <?php
    while ( have_posts() ) : the_post();

      get_template_part( 'flex' );

    endwhile; // End of the loop.
    ?>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer();
