<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

global $post;

get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <div class="content" ng-app="waltersApp" ng-controller="waltersCtrl">
      <div id="flex_row_1" class="flex-row-1 flex-row">
        <div class="wrap">
          <?php sideways_header(1, 'Project Portfolio'); ?>
          <div class="clearfix">
            <div class="select-filter multiple">
              <div class="form-group">
                <label>Filter: </label>
                <div class="select icon-caret">
                  <select ng-model="cat_filter">
                    <option selected value="">All Categories</option>
                    <?php foreach( get_terms('portfolio-category') as $category ): ?>
                    <option value="<?php echo $category->name; ?>"><?php echo $category->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <!-- posts_filter -->
                </div>
                <!-- .select -->
                <div class="select icon-caret">
                  <select ng-model="tag_filter">
                    <option selected value="">All Tags</option>
                    <?php foreach( get_terms('portfolio-tag') as $tag ): ?>
                      <option value="<?php echo $tag->name; ?>"><?php echo $tag->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <!-- posts_filter -->
                </div>
                <!-- .select -->
              </div>
              <!-- .form-group -->
            </div>
            <!-- .select-filter -->
            <h1 class="page-title">Portfolio</h1>
          </div>

          <i ng-if="!posts" class="icon-load"></i>

          <div class="grid news-grid">
            <?php get_template_part('template-parts/post/content', 'portfolio-piece-angular'); ?>
          </div>
        </div>
        <div ng-if="posts" class="load-more-wrap">
          <button class="btn" ng-click="loadMore()" ng-disabled="postsLimit > posts.length"  ng-bind="postsLimit > posts.length ? 'All Loaded' : 'Load More'">Load More</button>
        </div>
      </div>
    </div>
  </main><!-- #main -->
</div><!-- #primary -->
<script>
  (function ($, root, undefined) {

    $(function () {

      var app = angular.module('waltersApp', []);
      app.controller('waltersCtrl', function($scope, $http) {
        $http.get('<?php echo get_template_directory_uri(); ?>/scripts/portfolio.php')
          .then(function(response) {
          $scope.posts = response.data;
        });

        $scope.postsLimit = 6;

        $scope.loadMore = function(){
          $scope.postsLimit = $scope.postsLimit + 6;
        }

        $scope.updateFilter = function(tag){
          $scope.posts_filter = tag;
        }

      });
    });

  })(jQuery, this);
</script>
<?php get_footer();
