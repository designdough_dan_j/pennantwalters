<?php

/**
 * designdough functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function designdough_setup() {
  /*
   * Make theme available for translation.
   * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/designdough
   * If you're building a theme based on designdough, use a find and replace
   * to change 'designdough' to the name of your theme in all the template files.
   */
  load_theme_textdomain( 'designdough' );

  // Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
   */
  add_theme_support( 'post-thumbnails' );
  add_image_size( 'single-hero', 1500, 584, true );

  // This theme uses wp_nav_menu() in two locations.
  register_nav_menus( array(
    'header-menu'    => __( 'Header Menu', 'designdough' ),
    'footer-menu'    => __( 'Footer Menu', 'designdough' ),
  ) );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support( 'html5', array(
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
  ) );

  // Add theme support for selective refresh for widgets.
  add_theme_support( 'customize-selective-refresh-widgets' );

  // Add post type support for page excerpts
  add_post_type_support( 'page', 'excerpt' );

  /*
   * This theme styles the visual editor to resemble the theme style,
   * specifically font, colors, and column width.
   */
  add_editor_style( array( 'assets/css/editor-style.css', designdough_fonts_url() ) );

  /*
   * Add the designdough colour scheme
   */
  wp_admin_css_color(
    'designdough',
    __('designdough'),
    admin_url("css/colors/designdough/colors.css"),
    array('#242424', '#303030', '#99784d', '#bdbdbd'),
    array( 'base' => '#242424', 'focus' => '#fff', 'current' => '#fff' )
  );
}

add_action( 'after_setup_theme', 'designdough_setup' );

/**
 * Register custom fonts.
 */
function designdough_fonts_url() {
  $fonts_url = '';

  /**
   * Translators: If there are characters in your language that are not
   * supported by Libre Franklin, translate this to 'off'. Do not translate
   * into your own language.
   */
  $libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'designdough' );

  if ( 'off' !== $libre_franklin ) {
      $font_families = array();

      $font_families[] = 'Open Sans:400';
      $font_families[] = 'Montserrat:300,400,700';

      $query_args = array(
          'family' => urlencode( implode( '|', $font_families ) ),
          'subset' => urlencode( 'latin,latin-ext' ),
      );

      $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
  }

  return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since designdough 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function designdough_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'designdough-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'designdough_resource_hints', 10, 2 );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since designdough 1.0
 */
function designdough_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'designdough_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function designdough_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'designdough_pingback_header' );

/**
 * Enqueue scripts and styles.
 */
function designdough_scripts() {
  // Add custom fonts, used in the main stylesheet.
  wp_enqueue_style( 'designdough-fonts', designdough_fonts_url(), array(), null );

  // Theme stylesheet.
  wp_enqueue_style( 'designdough-style', get_stylesheet_uri() . '?v=1.1.6' );

  // Modernizr
  wp_enqueue_script('modernizr', get_theme_file_uri('/js/lib/modernizr.js'), array(), '3.5.0');

  // Theme javascript.
  wp_enqueue_script( 'designdough-js', get_theme_file_uri( '/js/main.min.js' ), array( 'jquery' ), '1.1', true );

}
add_action( 'wp_enqueue_scripts', 'designdough_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since designdough 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function designdough_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'designdough_content_image_sizes_attr', 10, 2 );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since designdough 1.0
 */
function designdough_custom_post_types() {
  // Portfolio
  register_post_type('portfolio-piece', // Register Custom Post Type
    array(
    'labels' => array(
      'name' => __('Portfolio', 'designdough'), // Rename these to suit
      'singular_name' => __('Portfolio Piece', 'designdough'),
      'add_new' => __('Add New', 'designdough'),
      'add_new_item' => __('Add New Portfolio Piece', 'designdough'),
      'edit' => __('Edit', 'designdough'),
      'edit_item' => __('Edit Portfolio Piece', 'designdough'),
      'new_item' => __('New Portfolio Piece', 'designdough'),
      'view' => __('View Portfolio Piece', 'designdough'),
      'view_item' => __('View Portfolio Piece', 'designdough'),
      'search_items' => __('Search Portfolio Piece', 'designdough'),
      'not_found' => __('No Portfolio Pieces found', 'designdough'),
      'not_found_in_trash' => __('No Portfolio Pieces found in Trash', 'designdough')
    ),
    'public' => true,
    'menu_icon' => 'dashicons-category',
    'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
    'has_archive' => true,
      'rewrite' => array('slug' => 'portfolio'),
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail'
    ), // Go to Dashboard Custom HTML5 Blank post for supports
    'can_export' => true, // Allows export in Tools > Export
    'taxonomies' => array(

    ) // Add Category and Post Tags support
  ));

  // Testimonials
  register_post_type('testimonial', // Register Custom Post Type
    array(
    'labels' => array(
      'name' => __('Testimonials', 'designdough'), // Rename these to suit
      'singular_name' => __('Testimonial', 'designdough'),
      'add_new' => __('Add New', 'designdough'),
      'add_new_item' => __('Add New Testimonial', 'designdough'),
      'edit' => __('Edit', 'designdough'),
      'edit_item' => __('Edit Testimonial', 'designdough'),
      'new_item' => __('New Testimonial', 'designdough'),
      'view' => __('View Testimonial', 'designdough'),
      'view_item' => __('View Testimonial', 'designdough'),
      'search_items' => __('Search Testimonial', 'designdough'),
      'not_found' => __('No Testimonials found', 'designdough'),
      'not_found_in_trash' => __('No Testimonials found in Trash', 'designdough')
    ),
    'public' => true,
    'menu_icon' => 'dashicons-testimonial',
    'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
    'has_archive' => true,
    'rewrite' => array('slug' => 'testionials'),
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail'
    ), // Go to Dashboard Custom HTML5 Blank post for supports
    'can_export' => true, // Allows export in Tools > Export
    'taxonomies' => array(

    ) // Add Category and Post Tags support
  ));

  // Testimonials
  register_post_type('plant', // Register Custom Post Type
    array(
    'labels' => array(
      'name' => __('Plant', 'designdough'), // Rename these to suit
      'singular_name' => __('Plant', 'designdough'),
      'add_new' => __('Add New', 'designdough'),
      'add_new_item' => __('Add New Plant', 'designdough'),
      'edit' => __('Edit', 'designdough'),
      'edit_item' => __('Edit Plant', 'designdough'),
      'new_item' => __('New Plant', 'designdough'),
      'view' => __('View Plant', 'designdough'),
      'view_item' => __('View Plant', 'designdough'),
      'search_items' => __('Search Plant', 'designdough'),
      'not_found' => __('No Plant found', 'designdough'),
      'not_found_in_trash' => __('No Plant found in Trash', 'designdough')
    ),
    'public' => true,
    //'menu_icon' => 'dashicons-testimonial',
    'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
    'has_archive' => true,
    'rewrite' => array ('slug' => 'plant-fleet'),
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail'
    ), // Go to Dashboard Custom HTML5 Blank post for supports
    'can_export' => true, // Allows export in Tools > Export
    'taxonomies' => array(

    ) // Add Category and Post Tags support
  ));
}

add_action('init', 'designdough_custom_post_types');

/**
 * Add custom taxonomies.
 *
 * @since designdough 1.0
 */

// create two taxonomies, genres and writers for the post type "book"
function designdough_custom_taxonomies() {

  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name'              => _x( 'Categories', 'taxonomy general name', 'designdough' ),
    'singular_name'     => _x( 'Category', 'taxonomy singular name', 'designdough' ),
    'search_items'      => __( 'Search Categories', 'designdough' ),
    'all_items'         => __( 'All Categories', 'designdough' ),
    'parent_item'       => __( 'Parent Category', 'designdough' ),
    'parent_item_colon' => __( 'Parent Category:', 'designdough' ),
    'edit_item'         => __( 'Edit Category', 'designdough' ),
    'update_item'       => __( 'Update Category', 'designdough' ),
    'add_new_item'      => __( 'Add New Category', 'designdough' ),
    'new_item_name'     => __( 'New Category Name', 'designdough' ),
    'menu_name'         => __( 'Categories', 'designdough' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'portfolio-category' ),
  );

  register_taxonomy( 'portfolio-category', array( 'portfolio-piece' ), $args );

  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name'                       => _x( 'Tags', 'taxonomy general name', 'designdough' ),
    'singular_name'              => _x( 'Tag', 'taxonomy singular name', 'designdough' ),
    'search_items'               => __( 'Search Portfolio Tags', 'designdough' ),
    'popular_items'              => __( 'Popular Portfolio Tags', 'designdough' ),
    'all_items'                  => __( 'All Portfolio Tags', 'designdough' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Portfolio Tag', 'designdough' ),
    'update_item'                => __( 'Update Portfolio Tag', 'designdough' ),
    'add_new_item'               => __( 'Add New Portfolio Tag', 'designdough' ),
    'new_item_name'              => __( 'New Portfolio Tag Name', 'designdough' ),
    'separate_items_with_commas' => __( 'Separate portfolio tags with commas', 'designdough' ),
    'add_or_remove_items'        => __( 'Add or remove portfolio tags', 'designdough' ),
    'choose_from_most_used'      => __( 'Choose from the most used portfolio tags', 'designdough' ),
    'not_found'                  => __( 'No wrportfolio tagsiters found.', 'designdough' ),
    'menu_name'                  => __( 'Tags', 'designdough' ),
  );

  $args = array(
    'hierarchical'          => false,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'portfolio-tag' ),
  );

  register_taxonomy( 'portfolio-tag', 'portfolio-piece', $args );

  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name'              => _x( 'Categories', 'taxonomy general name', 'designdough' ),
    'singular_name'     => _x( 'Category', 'taxonomy singular name', 'designdough' ),
    'search_items'      => __( 'Search Categories', 'designdough' ),
    'all_items'         => __( 'All Categories', 'designdough' ),
    'parent_item'       => __( 'Parent Category', 'designdough' ),
    'parent_item_colon' => __( 'Parent Category:', 'designdough' ),
    'edit_item'         => __( 'Edit Category', 'designdough' ),
    'update_item'       => __( 'Update Category', 'designdough' ),
    'add_new_item'      => __( 'Add New Category', 'designdough' ),
    'new_item_name'     => __( 'New Category Name', 'designdough' ),
    'menu_name'         => __( 'Categories', 'designdough' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'plant-category' ),
  );

  register_taxonomy( 'plant-category', array( 'plant' ), $args );
}

// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'designdough_custom_taxonomies', 0 );

/**
 * Handles JavaScript detection.
 *
 * Remove the <div> surrounding the dynamic navigation to cleanup markup.
 * Remove Injected classes, ID's and Page ID's from Navigation <li> items.
 * Remove invalid rel attribute values in the categorylist.
 *
 * @since designdough 1.0
 */
function my_wp_nav_menu_args($args = '') {
    $args['container'] = false;
    return $args;
}

function my_css_attributes_filter($var) {
    return is_array($var) ? array() : '';
}
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)

/**
 * Set designdough colour scheme as default
 *
 * @since designdough 1.0
 */

function change_admin_color($result) {
  return 'designdough';
}

add_filter('get_user_option_admin_color', 'change_admin_color');

/**
 * Remove invalid rel attribute
 *
 * @since designdough 1.0
 */
function remove_category_rel_from_category_list($thelist) {
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute

/**
 *
 * @since designdough 1.0
 */

remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );


/**
 * Handles JavaScript detection.
 *
 * Add page slug to body class - Credit: Starkers Wordpress Theme.
 *
 * @since designdough 1.0
 */
function add_slug_to_body_class($classes)
{
  global $post;

  $classes[] = sanitize_html_class($post->post_name);

  return $classes;
}

add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)

$social = array( 'Twitter', 'LinkedIn', 'Facebook', 'YouTube' );

foreach($social as $key){
$social_fields[] = array (
  'key' => sanitize_title($key).'_key',
  'label' => $key,
  'name' => sanitize_title($key),
  'type' => 'url',
);
}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
  'key' => 'social_key',
  'title' => 'Social',
  'fields' => $social_fields,
  'location' => array (
    array (
      array (
        'param' => 'page',
        'operator' => '==',
        'value' => 39,
      ),
    ),
  ),
));

endif;

function add_query_vars_filter( $vars ){
  $vars[] = 'tag';
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

function offices_func( $atts ) {
  $a = shortcode_atts( array(
    'limit' => -1,
    'full_address' => 1,
    'show_tel' => 1,
    'show_email' => 1,
  ), $atts );

  if( have_rows('offices', 39) ): $x = 1;

  $offices = '<div class="count-4">
            <div class="grid offices">';
  while ( have_rows('offices', 39) ) : the_row();
  $offices .= '<div class="col col-'.$x.'">
                <div class="office">
                  <h4>'.get_sub_field('office_name').'</h4>
                  <address>'.get_sub_field('office_address').'
                  </address>';

  if($a['show_tel'] === 1){
    $offices .= '<br/><p><a href="tel:+44:'.get_sub_field('office_tel').'"><span class="highlight">T</span> &nbsp; 0'.get_sub_field('office_tel').'</a></p>';
  }
  if($a['show_email'] === 1){
    $offices .= '<p><a href="mailto:'.get_sub_field('office_email').'"><span class="highlight">E</span> &nbsp; '.get_sub_field('office_email').'</a></p>';
  }

  $offices .= '</div>
              </div>';
  $x++; endwhile;
            $offices .= '</div>
          </div>';
endif;

  return $offices;
}
add_shortcode( 'offices', 'offices_func' );

function the_flex_classes($row){
  $classes = 'flex-row flex-'. get_row_layout() .' clearfix';

  if( get_sub_field('pull_quote') ){
    $classes .= ' with-pull-quote';
  }

  if( get_sub_field('quote_align') ){
    $classes .= ' ' . get_sub_field('quote_align');
  }

  echo 'id="flex_row_'.$row.'" class="'. $classes .'"';
}

function sideways_header($num, $title, $dynamic = 0){

  if( $dynamic === 1 ){
    $class = "dynamic";
  } else {
    $class = "";
  }

  echo '<div class="sideways-header '.$class.'"><span class="number">'.sprintf('%02d', $num).'</span><span class="text">'.$title.'</span></div>';
}

function fb_filter_query( $query, $error = true ) {

  if ( is_search() ) {
    $query->is_search = false;
    $query->query_vars[s] = false;
    $query->query[s] = false;

    // to error
    if ( $error == true )
        $query->is_404 = true;
  }
}

add_action( 'parse_query', 'fb_filter_query' );
add_filter( 'get_search_form', create_function( '$a', "return null;" ) );

function designdough_blog_load_more() {

  $args = array(
    'post_type' => $_POST['post_type'],
    'posts_per_page' => 6,
    'post_status' => 'publish'
  );

  if ($_POST['offset'] > 0) {
    $args['offset'] = $_POST['offset'];
  }

  $query = new WP_Query( $args );
  while ($query -> have_posts()) : $query -> the_post();

  if(isset($_POST['post_type'])){
    get_template_part( 'template-parts/post/content', $_POST['post_type'] );
  } else {
    get_template_part( 'template-parts/post/content' );
  }

  endwhile;

  die;
}
add_action('wp_ajax_ajax_blog_loadmore', 'designdough_blog_load_more');
add_action('wp_ajax_nopriv_ajax_blog_loadmore', 'designdough_blog_load_more');
