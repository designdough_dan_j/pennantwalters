<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage designdough
* @since 1.0
* @version 1.0
*/

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="content" ng-app="waltersApp" ng-controller="waltersCtrl">
			<div id="flex_1" class="flex-row-1 flex-row flex-news">
				<div class="wrap">
					<?php sideways_header(1, 'Plant'); ?>
					<div class="clearfix">
						<div class="select-filter">
							<div class="form-group">
								<label>Plant Type: </label>

								<div class="select icon-caret">
									<select ng-model="posts_filter.cat.name">
										<option selected value="">All</option>
										<?php
										foreach( get_terms('plant-category') as $category )
										{ ?>
											<option value="<?php echo $category->name; ?>"><?php echo $category->name; ?></option>
											<?php
										} ?>
									</select>
								</div>
							</div>
						</div>

						<h1 class="page-title pull-left">Plant Fleet</h1>
					</div>

					<i ng-if="!posts" class="icon-load"></i>

					<div class="grid news-grid">
						<?php get_template_part( 'template-parts/post/content', 'plant-angular' ); ?>
					</div>

					<!-- <div ng-if="posts" class="load-more-wrap">
						<button class="btn" ng-click="loadMore()" ng-disabled="postsLimit > posts.length"  ng-bind="postsLimit > posts.length ? 'All Loaded' : 'Load More'">Load More</button>
					</div> -->
				</div>
			</div>

			<!-- <div class="plant-feature">
				<?php //get_template_part( 'template-parts/post/content', 'plant-card-angular' ); ?>
			</div> -->
		</div>
	</main><!-- #main -->
</div><!-- #primary -->
<script>
(function ($, root, undefined) {

	$(function () {

		var app = angular.module('waltersApp', []);
		app.controller('waltersCtrl', function($scope, $http) {
			$http.get('<?php echo get_template_directory_uri(); ?>/scripts/plant.php')
			.then(function(response) {
				$scope.posts = response.data;
			});

			$scope.postsLimit = 6;

			<?php if( get_query_var('tag') ): ?>
			$scope.posts_filter = '<?php echo get_query_var('tag'); ?>'
			<?php endif; ?>

			$scope.loadMore = function() {
				$scope.postsLimit = $scope.postsLimit + 6;
			}

			$scope.updateFilter = function( tag ) {
				$scope.posts_filter = tag;
			}
		});

		$('.close-card').click( function () {
			$('.plant-feature').fadeOut(250);
			$('body').removeClass('no-scroll');
		});
	});

})(jQuery, this);
</script>

<?php get_footer();
