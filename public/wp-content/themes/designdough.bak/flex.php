<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

// check if the flexible content field has rows of data
if( have_rows('flex') ):
  // loop through the rows of data
  while ( have_rows('flex') ) : the_row();

    if( get_row_layout() == 'contact' ):

      get_template_part('flex/contact'); // Banners

    elseif( get_row_layout() == 'map' ):

      get_template_part('flex/map'); // Banners

    elseif( get_row_layout() == 'news' ):

      get_template_part('flex/news'); // Banners

    elseif( get_row_layout() == 'pages' ):

      get_template_part('flex/pages'); // Banners

    elseif( get_row_layout() == 'portfolio' ):

      get_template_part('flex/portfolio'); // Banners

    elseif( get_row_layout() == 'text' ):

      get_template_part('flex/text'); // Text

    elseif( get_row_layout() == 'shrek'):

      get_template_part('flex/shrek'); // Text

    elseif(get_row_layout() == 'shrek-quick' ):

      get_template_part('flex/shrek-quick-links'); // Text

    elseif( get_row_layout() == 'video' ):

      get_template_part('flex/video'); // Text

    endif;

  endwhile;
else :
    // no layouts found
endif;
