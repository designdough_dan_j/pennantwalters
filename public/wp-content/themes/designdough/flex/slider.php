<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index(); ?>
<div <?php the_flex_classes($row); ?>>
    <div class="wrap flex-video">
        <?php sideways_header($row, get_sub_field('sideways_header')); ?>
        <h2 class="flex-title"><?php the_sub_field('title'); ?></h2>

        <div class="content">
            <div id="main_slider" class="slick-slider <?php if(get_sub_field('smaller')) { ?>smaller<?php } ?>">
                <?php if(get_row('slides')) {
                    while(have_rows('slides')) {
                        the_row();
                        $image = get_sub_field('image')?>
                        <div class="video-wrap">
                            <img src="<?php echo $image['url'];  ?>" />
                        </div>

                    <?php };
                }; ?>
            </div>
        </div>
    </div>
</div>
<script>
  (function ($, root, undefined) {
    $(function () {
      $('#main_slider').slick({
        slidesToShow: 1,
        autoplay: true,
      })
    });


  })(jQuery, this);
</script>