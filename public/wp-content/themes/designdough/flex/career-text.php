<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();

?>

<!-- NPM run watch not working -->
<style>
    .career-text {
        background: #FFFFFF;
        padding: 20px;
    }
</style>

<div <?php the_flex_classes($row); ?>>
    <div class="wrap">
        <?php sideways_header($row, get_sub_field('sideways_header')); ?>

        <div class="career-text">
            <h2 class="flex-title"><?php the_sub_field('title'); ?></h2>
<!--            <h4 class="flex-title">--><?php //the_sub_field('salary'); ?><!--</h4>-->
            <div class="content">
                <div style="width:50%;"><hr></div>

                <?php
                if (have_rows('section')) {
                    while (have_rows('section')) {
                        the_row();
                        ?>
                        <h3><?php the_sub_field('title'); ?></h3>
                        <p><?php the_sub_field('content'); ?></p>

                        <?php
                        wp_reset_postdata();
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php

//$password = hash("sha256",'_cw_!FuMfisBu$^8Dh$%^6dW&^&^*4GGfV&$@'.'jbPass17');
//echo $password;