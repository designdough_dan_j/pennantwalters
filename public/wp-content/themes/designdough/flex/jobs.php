<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();
?>

<!-- NPM run watch not working -->
<style>
    .jobs__title-wrap {
        display: flex;
        flex-direction: row;

        flex-wrap: wrap;
        justify-content: space-between;
    }

    .flex-title {
        display: flex;
        flex-direction: column;
    }

    .jobs__filter-row {
        display: flex;
    }

    .jobs__filter {
        display: flex;
        flex-direction: column;
        width: 30%;
    }

    @media (max-width: 992px) {
        .jobs__filter
        {
            width: 100%;
            margin-bottom: 30px;
        }
    }

    .jobs__filter-label {
        display: flex;
        flex-direction: column;
        width: 25%;
        margin-right: 10px;
    }

    .jobs__filter-list {
        display: flex;
        flex-direction: column;
        border: 1px solid #57595b;
        background: none;
    }

    .jobs__wrap {
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
    }

    .jobs__group {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        /*justify-content: space-between;*/
        width: 100%;
    }

    .jobs__item {
        display: flex;
        flex-direction: column;
        width: 32%;
        margin-bottom: 3rem;
        margin-right: 2rem;
    }

    @media (max-width: 992px) {
        .jobs__item {
            width: 100%;
        }
    }

    .jobs__cats {
        text-transform: uppercase;
    }

    .jobs__hide {
        display: none;
    }

    .jobs__show-more {
         margin: 30px auto;
    }

    .jobs__show-more img {
        width: 50px;
        cursor: pointer;
    }
</style>

<div <?php the_flex_classes($row); ?>>
    <div class="jobs wrap">
        <?php sideways_header($row, get_sub_field('sideways_header')); ?>
        <div class="content">
            <div class="jobs__title-wrap">
                <h2 class="flex-title"><?php the_sub_field('title'); ?></h2>

                <?php $terms = get_terms(array(
                    'taxonomy' => 'job-types',
                    'hide_empty' => false,
                )); ?>

                <?php if (is_array($terms)) { ?>
                    <div class="jobs__filter">
                        <div class="jobs__filter-row">
                            <div class="jobs__filter-label">
                                Job Type:
                            </div>

                            <select class="jobs__filter-list">
                                <option value="0">Select Job Type</option>

                                <?php for ($i = 0; $i < sizeof($terms); $i++) { ?>
                                    <option value="<?php echo $terms[$i]->slug; ?>">
                                        <?php echo $terms[$i]->name; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="jobs__wrap">
                <?php
                $args = array(
                    'post_type' => 'jobs',
                    'posts_per_page' => -1,
                    'order' => 'ASC'
                );

                $the_query = new WP_Query($args);
                if ($the_query->have_posts()) {
                    $i = 0;
                    $groupCount = 0;
                    while ($the_query->have_posts()) {
                        $the_query->the_post();

                        $termStr = '';
                        $itemTerms = get_the_terms(get_the_ID(), 'job-types');

                        if ($itemTerms) {
                            for ($j = 0; $j < sizeof($itemTerms); $j++) {
                                $termStr .= $itemTerms[$j]->slug . " ";
                            }
                        }

                        if ($i % 6 == 0) { ?>
                            <?php $hideClass = $i == 0 ? "" : "jobs__hide"; ?>
                            <div class="jobs__group group-<?php echo $groupCount . " " . $hideClass; ?>">
                            <?php $groupCount++; ?>
                        <?php } ?>

                            <div class="jobs__item <?php echo $itemTerms[0]->slug; ?>">
                                <div class="news-item">
                                    <div class="jobs__cats tags">
                                        <?php echo $itemTerms[0]->name; ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                                    <?php the_excerpt(); ?>
                                    <a href="<?php the_permalink(); ?>" class="hide-text icon-arrow-right"
                                       tabindex="0"><span class="screen-reader-text">Read More</span></a>
                                </div>
                            </div>
                        <?php if ($i != 0 && ($i % 5 == 0)) { ?>
                            </div>
                        <?php }
                        $i++;
                    }
                    wp_reset_postdata();
                }
                ?>

            </div>
            <div class="jobs__show-more" data-show-count="0">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/load-more.svg" alt="Load More">
            </div>
        </div>
    </div>
</div>

<script>
    (function ($, root, undefined) {
        $(function () {
            $('.jobs__filter-list').on('change', function() {

                let items = $('.jobs__item').toArray();

                for (let i = 0; i < items.length; i++) {
                    if ($(this).val() != 0) {
                        if ($(items[i]).hasClass($(this).val())) {
                            $(items[i]).fadeIn();
                        } else {
                            $(items[i]).fadeOut();
                        }
                    }
                    else {
                        for (let i = 0; i < items.length; i++) {
                            $(items[i]).fadeIn();
                        }
                    }
                }
            });

            $('.jobs__show-more').on('click', function() {
                let count = $(this).data('show-count');
                count++;
                console.log(count);

                $('.group-' + count).removeClass('jobs__hide');
                $(this).data('show-count', count);

                count++;

                if ($('.group-' + count).length == 0) {
                    $(this).addClass('jobs__hide');
                }
            });
        });
    })(jQuery, this);
</script>