<?php
/**
 * The flexible content of the theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();

?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- NPM run watch not working -->
<style>
    .btn-wrap {
        width: 200px;
    }

    form {
        line-height: 20px;
    }

    form .btn {
        margin-top: 50px;
    }

    .form-group {
        display: flex;
    }

    .form-group label {
        margin-right: 100px;
        width: 120px;
    }

    .employmentLabel {
        margin-top: 20px;
    }

    .radio {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;

        padding-left: 40px;
        padding-top: 20px;
    }

    .input-wrap {
        width: 50%;
        margin-bottom: 20px;

        display: flex;
        flex-direction: row;
    }

    @media (max-width: 992px) {
        .input-wrap {
            width: 100%;
            margin-bottom: 20px;
        }
    }

    input[type=text], input[type=email], input[type=tel] {
        width: 55%;
    }

    .radio label {
        margin-top: -7px;
    }

    .radio input {
        margin-right: 20px;
    }

    .date-slash {
        margin: 0 10px;
    }

    .date-input {
        max-width: 50px;
        max-height: 30px;
    }

    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .btn-upload {
        background: #878787;
        color: white;
        padding: 8px 20px;
        font-size: 16px;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 2rem;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }
</style>

<div <?php the_flex_classes($row); ?>>
    <div class="wrap">
        <?php sideways_header($row, get_sub_field('sideways_header')); ?>

        <h2><?php the_sub_field('title'); ?></h2>

        <div class="content form-wrap">
            <div class="form-response">
                test
                <div class="inner"></div>
            </div>

            <form id="apply_form" method="POST" enctype='multipart/form-data'
                  action="<?php echo get_template_directory_uri(); ?>/scripts/send-apply.php">
                <div class="cols count-1">
                    <div class="">
                        <div class="form-group">
                            <label class="">Full Name</label>
                            <input required type="text" id="full_name" name="full_name"/>
                        </div>

                        <div class="form-group">
                            <label class="">Email Address</label>
                            <input required type="email" id="email_address" name="email_address"/>
                        </div>

                        <div class="form-group">
                            <label class="">Contact No.</label>
                            <input required type="tel" id="contact_no" name="contact_no"/>
                        </div>

                        <div class="form-group">
                            <label class="employmentLabel">Current employment status</label>

                            <div class="radio">
                                <div class="input-wrap">
                                    <input required type="radio" id="employed" name="emp_status" value="employed"/>
                                    <label class="">Employed</label>
                                </div>

                                <div class="input-wrap">
                                    <input required type="radio" id="self-employed" name="emp_status"
                                           value="self-employed"/>
                                    <label class="">Self-Employed</label>
                                </div>

                                <div class="input-wrap">
                                    <input required type="radio" id="unemployed" name="emp_status" value="unemployed"/>
                                    <label class="">Unemployed</label>
                                </div>

                                <div class="input-wrap">
                                    <input required type="radio" id="student" name="emp_status" value="student"/>
                                    <label class="">Student</label>
                                </div>
                            </div>

                        </div>

<!--                        <div class="form-group">-->
<!--                            <label class="">Available start date</label>-->
<!---->
<!--                            <input required type="number" class="date-input" max="31" min="1" id="date_day"-->
<!--                                   name="date_day"/> <span class="date-slash">/</span>-->
<!--                            <input required type="number" class="date-input" max="12" min="1" id="date_month"-->
<!--                                   name="date_month"/> <span class="date-slash">/</span>-->
<!--                            <input required type="number" class="date-input" max="--><?php //echo date('Y') + 1; ?><!--"-->
<!--                                   min="--><?php //echo date('Y'); ?><!--" id="date_year" name="date_year"/>-->
<!--                        </div>-->

                        <div class="form-group">
                            <div class="upload-btn-wrapper">
                                <label class="">Upload your CV</label>
                                <button class="btn-upload">Upload</button>
                                <input type="file" required name="cv-upload"/>

                            </div>
                        </div>

                        <input type="text" name="name" style="width: 0; height: 0; opacity: 0;"/>

                        <div class="g-recaptcha" data-sitekey="6LfOlMcUAAAAAM5mUUqfom2bRdsefEm6LHwCJ24c"></div>

                        <div class="btn-wrap">
                            <button class="btn">Send Message</button>
                        </div>
                    </div>

<!--                    <div class="col">-->
<!--                        <div class="form-group full-height">-->
<!--                            <textarea class="full-height" required-->
<!--                                      placeholder="Please tell us why you're the most suitable candidate for the job (include experience, qualifications and references):"-->
<!--                                      id="enquiry" name="enquiry"></textarea>-->
<!--                            <label class="hidden">Enquiry</label>-->
<!--                        </div>-->
<!--                    </div>-->

                </div>
            </form>

            <?php //phpinfo(); ?>
        </div>
    </div>
</div>
<script>
    (function ($, root, undefined) {
        $(function () {
            $('#contact_form_<?php echo get_row_index(); ?>').validate({
                errorPlacement: function (error, element) {
                    error.appendTo(element.parent('.form-group'));
                },
                messages: {
                    full_name: "Please specify your name",
                    email_address: {
                        required: "We need your email address to contact you",
                        email: "Pleae enter a valid email address"
                    },
                    enquiry: "Enter your message"
                },
                submitHandler: function (form) {
                    event.preventDefault();

                    if ($(form).find('input[name="name"]').val() != '') {
                        $('.form-response').fadeIn(250).find('.inner').html('Spam detected');
                        return;
                    }

                    var wrap = $(form).parent('.form-wrap');

                    $.ajax({
                        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url: $(form).attr('action'), // the url where we want to POST
                        data: $(form).serialize(), // our data object
                    })
                        .done(function (data) {
                            $('.form-response').fadeIn(250).find('.inner').html(data);

                            setTimeout(function () {
                                $('.form-response').fadeOut(250);
                                $(form)[0].reset();
                            }, 10000);
                        });
                }
            });
        });
    })(jQuery, this);
</script>
