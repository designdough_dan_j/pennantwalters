<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();

?>
<div <?php the_flex_classes($row); ?>>
  <div class="wrap">
    <?php sideways_header($row, get_sub_field('sideways_header')); ?>
    <?php
    
    $post_objects = get_sub_field('pages');

    if( $post_objects ): ?>
    <div class="grid page-feed">
      <?php $i = 1; ?>
      <?php foreach( $post_objects as $post): ?>
      <?php setup_postdata($post); ?>
      <div class="col col-<?php echo $i; ?>">
        <div class="page-feed-box" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
          <i class="plus-sign" aria-hidden="true"></i>
          <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
          <ul class="sub-menu new-page-system">
            <?php
            // check if the repeater field has rows of data
            if( have_rows('services_repeater', $post->ID) ): $x = 1;
                // loop through the rows of data
                while ( have_rows('services_repeater', $post->ID) ) : the_row(); if( $x != 1 ): ?>
                        <li><a class="newclass <?php echo get_sub_field('custom_url') ?>" href="<?php echo get_the_permalink($post->ID); ?>#<?php echo sanitize_key(get_sub_field('name')); ?>"><?php the_sub_field('name'); ?></a></li>


            <?php
                endif;
            $x++;

                endwhile;

            endif;

            ?>
          </ul>
        </div>
        <div class="spider-nav no-bullets">
          <ul>
            <?php

            // check if the repeater field has rows of data
            if( have_rows('services_repeater', $post->ID) ): $x = 1;

                // loop through the rows of data
                while ( have_rows('services_repeater', $post->ID) ) : the_row();
            ?>
            <li class="item-<?php echo $x; ?>">
                        <?php if(get_sub_field('custom_url')) { ?>
                            <a href="<?php echo get_sub_field('custom_url');?>">
                        <?php } else { ?>
                            <a href="<?php echo get_the_permalink($post->ID); ?>#<?php echo sanitize_key(get_sub_field('name')); ?>">
                        <?php } ?>
                <i class="plus-sign" aria-hidden=""></i>
                <span class="title"><?php the_sub_field('name'); ?></span>
              </a>
              <span class="line"></span>
            </li>
            <?php
            $x++;

                endwhile;

            endif;

            ?>
          </ul>
        </div>
      </div>
      <?php $i++; endforeach; ?>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    <?php endif; ?>
  </div>
</div>