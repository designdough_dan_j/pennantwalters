<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();

?>
<div <?php the_flex_classes($row); ?>>
  <div class="wrap">
    <?php sideways_header($row, get_sub_field('sideways_header')); ?>
    <div class="content">
      <div class="video-wrap">
        <div class="replay-overlay"><a class="replay-btn"><i class="icon-replay" aria-hidden="true"></i>Replay</a></div>
        <video autoplay loop muted playsinline>
          <source src="<?php the_sub_field('vimeo'); ?>" type="video/mp4">
        </video>
      </div>
    </div>
  </div>
</div>