<?php
/**
 * The flexible content of the  theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$row = get_row_index();

?>
<div <?php the_flex_classes($row); ?>>
  <div class="wrap">
    <?php sideways_header($row, get_sub_field('sideways_header')); ?>
    <div class="content">
      <h2 class="flex-title"><?php the_sub_field('title'); ?></h2>
      <div id="news_slider" class="slick-slider">
        <?php
        
        global $post;
        $args = array( 'posts_per_page' => get_sub_field('posts_per_page') );
        
        $posts = get_posts( $args );
        foreach ( $posts as $post ) : setup_postdata( $post ); ?>
        <div class="slick-slide">
          <div class="news-item">
            <div class="tags no-bullets">
              <ul class="clearfix">
                <?php foreach( get_the_tags() as $tag ): ?>
                <li><a href="<?php echo get_permalink( get_option('page_for_posts' ) ); ?>?tag=<?php echo $tag->name; ?>"><?php echo $tag->name; ?></a></li>
                <?php endforeach; ?>
              </ul>
            </div>
            <a class="thumb" href="<?php the_permalink(); ?>" style="background-image:url(<?php the_post_thumbnail_url(); ?>)"></a>
            <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="hide-text icon-arrow-right" tabindex="0"><span class="screen-reader-text">Read More</span></a>
          </div>
        </div>
        <?php endforeach;
        wp_reset_postdata();?>
      </div>
    </div>
  </div>
</div>
<script>
(function ($, root, undefined) {
	
  $(function () {

    $('#news_slider').slick({
      slidesToShow: 5,
      responsive: [
        {
          breakpoint: 2560,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            adaptiveHeight: true,
          }
        }
      ]
    });
  });
	
})(jQuery, this);
</script>