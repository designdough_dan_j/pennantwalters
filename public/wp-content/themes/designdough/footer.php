<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

global $flex_row;
global $social;

?>
      <footer id="footer">
        <div class="wrap">
          <?php $offices = get_field('offices', 39); ?>
          <div class="main-office">
            <a class="logo" href="<?php echo home_url(); ?>"><?php echo bloginfo('sitename'); ?></a>
            <p><a href="tel:+44:<?php echo $offices[0]['office_tel']; ?>"><span class="highlight">T</span> &nbsp; 0<?php echo $offices[0]['office_tel']; ?></a></p>
            <p><a href="mailto:<?php echo $offices[0]['office_email']; ?>"><span class="highlight">E</span> &nbsp; <?php echo $offices[0]['office_email']; ?></a></p>
          </div>
          <?php echo do_shortcode('[offices]'); ?>
        </div>
        <div class="social-links no-bullets">
            <ul class="clearfix">
              <?php foreach( $social as $key ): ?>
              <li><a title="<?php echo $key; ?>" class="hide-text icon-<?php echo sanitize_title($key); ?>" target="_blank" href="<?php the_field(sanitize_title($key), 39); ?>"><?php echo $key; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        <div class="copyright">
          <div class="wrap">
            &copy; <?php echo bloginfo('name'); ?> <?php echo date('Y'); ?> &nbsp; | &nbsp; All Rights Reserved
            <div class="nav">
              <a href="<?php echo get_the_permalink(786); ?>"><?php echo get_the_title(786); ?></a> &nbsp; | &nbsp; <a href="<?php echo get_the_permalink(788); ?>"><?php echo get_the_title(788); ?></a><!-- &nbsp; | &nbsp; <a>Design By Designdough</a> -->
            </div>
          </div>
        </div>
      </footer>
    </div>
    </div>
    <!-- /Site Wrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>

      (function($, root, undefined) {
    $(document).ready(function() {
        $('#projectcategoryfilter').change(function() {
            var filter = $('#filter-projects');
            $.ajax({
                url: filter.attr('action'),
                data: filter.serialize(), // form data
                type: filter.attr('method'), // POST
                beforeSend: function(xhr) {
                    $('.loading-icon-3').removeClass('hidden'); // changing the button label
                },

                success: function(data) {
                    $('.loading-icon-3').addClass('hidden'); // changing the button label
                    $('#response-3').html(data); // insert data
                }
            });

            return false;
        });
        /**
         * end
         */
    });
})(jQuery, this);
      </script>
    <?php wp_footer(); ?>
  </body>
</html>
