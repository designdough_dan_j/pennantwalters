<div id="history_extra" class="extra-tab-content">
  <h2>Walters Timeline</h2>
  <div id="timeline_list" class="timeline clearfix no-bullets">
    <span class="line horizontal"></span>
    <ul>
      <?php
      
      $timeline = get_field('timeline');

      // check if the repeater field has rows of data
      if( have_rows('timeline') ): $x = 1;

          // loop through the rows of data
          while ( have_rows('timeline') ) : the_row();
      $img = get_sub_field('timeline_img');
      ?>
      <li class="item-<?php echo $x; ?> <?php if($x % 2 == 0) { echo "left "; } else { echo "right "; } if($x > (count($timeline) - 2)){ echo "no-margin "; }  if($img) { echo "with-img "; } ?>">
        <div class="timeline-item clearfix">
          <div class="timeline-desc">
            <h4 class="timeline-year"><?php the_sub_field('timeline_year'); ?></h4>
            <?php the_sub_field('timeline_content'); ?>
            <span class="more" data-target="#timeline_full_<?php echo $x; ?>"><i class="plus-sign" aria-hidden="false"></i>Read More</span>
            <?php if( $img ): ?>
            <div class="thumb" style="background-image:url(<?php echo $img['url']; ?>);"></div>
            <?php endif; ?>
          </div>
          <span class="line vertical"><span class="ball"></span></span>
        </div>
      </li>
      <div id="timeline_full_<?php echo $x; ?>" class="timeline-content-full">
        <a class="close" href="#timeline_full_<?php echo $x; ?>">&#10006;</a>
        <h4 class="timeline-year"><?php the_sub_field('timeline_year'); ?></h4>
        <?php the_sub_field('timeline_content'); ?>
        <?php the_sub_field('timeline_content_more'); ?>
      </div>
      <?php
          $x++;

          endwhile;

      endif;

      ?>
    </ul>
  </div>
  <script>
  (function ($, root, undefined) {
    $(function () {
        for (i = 1; i < $('.timeline-item').length; i++) {

          var tween = new TimelineMax();

          if( $('.item-' + i).hasClass('left') ){
            settings = { marginRight: 0, };
          } else {
            settings = { marginLeft: 0, };
          }

          tween.from(".item-" + i + " .ball", 3, { transform: 'scale(0)' }, 0);
          tween.from(".item-" + i + " .line", 1, { width: 0 }, 0);
          tween.from(".item-" + i + " .timeline-item", 1, { opacity: 0, }, 0);
          tween.from(".item-" + i + " .timeline-desc", 1, settings, 0);

          // build scene
          var scene  = new ScrollMagic.Scene({
            triggerElement: "#timeline_list .item-" + i,
            triggerHook: 1,
          })
          .setTween(tween)
          .addTo(controller);
        }
      });
    })(jQuery, this);
  </script>
</div>