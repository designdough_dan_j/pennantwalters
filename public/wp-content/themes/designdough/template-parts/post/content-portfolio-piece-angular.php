<div class="col col-{{ $index + 1 }}" data-cats="{{ x.cats }}" ng-repeat="x in posts | filter: { cats: cat_filter } |filter: { tags: tag_filter } | limitTo: postsLimit">
  <a class="portfolio-item" href="{{ x.link }}">
    <div class="item-info">
      <h3>{{ x.post_title }}</h3>
      <p>{{ x.project }}</p>
      <span class="icon-arrow-right"></span>
    </div>
    <span class="thumb" style="background-image:url('{{ x.img }}');"></span>
  </a>
</div>