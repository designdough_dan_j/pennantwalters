<div class="plant-card">
	<img class="close-card" src="<?php echo get_template_directory_uri();?>/img/close.svg">

	<p class="plant-header">
		{{ single_post.cat.name }}
	</p>

	<div class="plant-gallery" ng-if="single_post.image_count >= 1">
		<div class="feature-image" style="background-image: url({{ single_post.fields.images[0].url }})"></div>
		<div class="normal-image" ng-if="single_post.image_count > 1" style="background-image: url({{ single_post.fields.images[1].url }})"></div>
		<div class="normal-image" ng-if="single_post.image_count > 2" style="background-image: url({{ single_post.fields.images[2].url }})"></div>
	</div>

	<div class="plant-stats">
		<h3>
			{{ single_post.post_title }}

			<div class="right-items">
				<span>Fleet: <b>{{ single_post.cat.count }}</b></span>

				<span ng-if="single_post.fields.download">
					<a href="{{ single_post.fields.download.url }}">Download: <img src="<?php echo get_template_directory_uri();?>/img/download.svg"></a>
				</span>
			</div>
		</h3>
		<hr>

		<div class="stats">
			<p>
				max power
				<span>{{ single_post.fields.max_power }} hp</span>
			</p>

			<p>
				max torque
				<span>{{ single_post.fields.max_torque }} nm</span>
			</p>

			<p>
				operating weight
				<span>{{ single_post.fields.operating_weight }} kg</span>
			</p>

			<p>
				static tipping weight
				<span>{{ single_post.fields.static_tipping_weight }} kg</span>
			</p>

			<p>
				max speed
				<span>{{ single_post.fields.max_speed }} mph</span>
			</p>

			<p>
				bucket capacity
				<span>{{ single_post.fields.bucket_capacity }} m<sup>3</sup></span>
			</p>
		</div>

		<div class="content">
			{{ single_post.post_content }}
		</div>
	</div>

	<a href="/contact" class="btn">Send Enquiry</a>
</div>
