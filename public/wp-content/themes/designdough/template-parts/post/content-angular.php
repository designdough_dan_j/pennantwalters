<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="col col-{{ $index + 1 }}" ng-repeat="x in posts | filter: { cats: cat_filter } | filter: { tags: tag_filter } | limitTo: postsLimit">
  <div class="news-item">
    <div class="tags no-bullets">
      <ul class="clearfix">
        <li ng-repeat="tag in x.tags"><a ng-click="updateFilter(tag)">{{ tag.name }}</a></li>
      </ul>
    </div>
    <a class="thumb" href="{{ x.link }}" style="background-image:url({{ x.img }})"></a>
    <h3>{{ x.post_title }}</h3>
    <p>{{ x.excerpt }}</p>
    <a href="{{ x.link }}" class="hide-text icon-arrow-right" tabindex="0"><span class="screen-reader-text">Read More</span></a>
  </div>
</div>