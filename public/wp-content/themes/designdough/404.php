<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <div id="flex_row_1" class="flex-row flex-text">
      <div class="wrap">
        <h1>404 Error</h1>
        <p>The page you were looking for cannot be found</p>
        <h3><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Click here</a> to return to the homepage</h3>
      </div>
    </div>

  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer();