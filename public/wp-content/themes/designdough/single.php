<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="wrap">
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <div class="article-wrap">
      <div class="tags">
        <?php the_tags( '', ' | ', '' ); ?>&nbsp;
      </div>
      <div class="single-hero">
        <?php 

        if( has_post_thumbnail() ){
           the_post_thumbnail('single-hero');
         }

        ?>
      </div>
        <article>
          <h1><?php the_title(); ?></h1>
          <?php
            /* Start the Loop */
            while ( have_posts() ) : the_post();
              the_content();
            endwhile; // End of the loop.
          ?>
          <a class="icon-arrow-left" href="<?php echo get_permalink( get_option('page_for_posts' ) ); ?>"></a>
        </article>
      </div>
    </main><!-- #main -->
  </div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();