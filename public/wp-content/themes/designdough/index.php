<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <div class="content" ng-app="waltersApp" ng-controller="waltersCtrl">

      <div id="flex_1" class="flex-row-2 flex-row flex-news">
        <div class="wrap">
          <?php sideways_header(1, 'Featured Article'); ?>
          <?php $feat = get_posts(array('posts_per_page' => 1)); ?>
          <div class="news-item featured clearfix">
            <div class="tags">
              <?php the_tags( '', ' | ', '' ); ?>&nbsp;
            </div>
            <a class="thumb" href="<?php the_permalink(); ?>" style="background-image:url(<?php the_post_thumbnail_url(); ?>)"></a>
            <div class="desc">
              <strong class="feat">Featured Article</strong>
              <a href="<?php echo get_permalink($feat[0]->ID); ?>"><h3><?php echo $feat[0]->post_title; ?></h3></a>
              <?php echo wpautop($feat[0]->post_excerpt); ?>
              <a href="<?php echo get_the_permalink($feat[0]->ID); ?>" class="hide-text icon-arrow-right" tabindex="0"><span class="screen-reader-text">Read More</span></a>
            </div>
          </div>
        </div>
      </div>

      <div id="flex_2" class="flex-row-2 flex-row flex-news">
        <div class="wrap">
          <?php sideways_header(2, 'Latest News'); ?>
          <div class="clearfix">
            <div class="select-filter multiple">
              <div class="form-group">
                <label>Filter: </label>
                <div class="select icon-caret">
                  <select ng-model="cat_filter">
                    <option selected value="">All Categories</option>
                    <?php foreach( get_terms('portfolio-category') as $category ): ?>
                    <option value="<?php echo $category->name; ?>"><?php echo $category->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <!-- posts_filter -->
                </div>
                <!-- .select -->
                <div class="select icon-caret">
                  <select ng-model="tag_filter">
                    <option selected value="">All Tags</option>
                    <?php foreach( get_terms('portfolio-tag') as $tag ): ?>
                      <option value="<?php echo $tag->name; ?>"><?php echo $tag->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <!-- posts_filter -->
                </div>
                <!-- .select -->
              </div>
              <!-- .form-group -->
            </div>
            <!-- .select-filter -->
            <h1 class="page-title pull-left"><?php single_post_title(); ?></h1>
          </div>

          <i ng-if="!posts" class="icon-load"></i>

          <div class="grid news-grid">
            <?php get_template_part( 'template-parts/post/content', 'angular' ); ?>
          </div>
          <div ng-if="posts" class="load-more-wrap">
            <button class="btn" ng-click="loadMore()" ng-disabled="postsLimit > posts.length"  ng-bind="postsLimit > posts.length ? 'All Loaded' : 'Load More'">Load More</button>
          </div>
        </div>

      </div>
    </div>


  </main><!-- #main -->
</div><!-- #primary -->
<script>
  (function ($, root, undefined) {
	
    $(function () {
      
      var app = angular.module('waltersApp', []);
      app.controller('waltersCtrl', function($scope, $http) {
        $http.get('<?php echo get_template_directory_uri(); ?>/scripts/posts.php')
          .then(function(response) {
          $scope.posts = response.data;
        });
        
        $scope.postsLimit = 6;
        
        <?php if( get_query_var('tag') ): ?>
        $scope.posts_filter = '<?php echo get_query_var('tag'); ?>'
        <?php endif; ?>
        
        
        $scope.loadMore = function(){
          $scope.postsLimit = $scope.postsLimit + 6;
        }
        
        $scope.updateFilter = function(tag){
          $scope.posts_filter = tag;
        }
        
      });
    });
    
  })(jQuery, this);
</script>
<?php get_footer();