<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <div class="wrap">
      <?php sideways_header(1, 'Project'); ?>
      <div class="single-hero">
        <?php 

        if( has_post_thumbnail() ){
           the_post_thumbnail('single-hero');
         }

        ?>
      </div>
      <div class="project-details">
        <div class="gallery no-bullets">
          <?php 

          $images = get_field('gallery');

          if( $images ): ?>
            <ul>
              <?php foreach( $images as $image ): ?>
                <li>
                  <figure class="gallery-item">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>">
                    <?php if( $image['caption'] ): ?>
                      <figcaption>
                        <div class="inner">
                          <?php echo $image['caption']; ?>
                        </div>
                      </figcaption>
                    <?php endif; ?>
                  </figure>
                </li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        </div>
        <div class="content">
          <div class="portfolio-header">
            <?php
            
            if(get_field('pdf')): $pdf = get_field('pdf'); ?>
            <a class="pdf-download" href="<?php echo $pdf['url']; ?>">Download PDF</a>
            <?php endif; ?>
            <h1><?php the_title(); ?></h1>
          </div>
          <div class="project-spec">
            <p><strong>Client:</strong> <?php the_field('project_client'); ?></p>
            <p><strong>Value:</strong> £<?php the_field('project_value'); ?></p>
            <p>
              <strong>At-a-Glance:</strong> 
              <?php if( have_rows('at_a_glance') ): ?>
            <ul>
              <?php while ( have_rows('at_a_glance') ) : the_row(); ?>
              <li><?php the_sub_field('content'); ?></li>
              <?php endwhile; ?>
            </ul>
            <?php endif; ?>
            </p>
          </div>
        <?php
        /* Start the Loop */
        while ( have_posts() ) : the_post();
        the_content();
        endwhile; // End of the loop.
      ?>
        </div>
      </div>
    <br/>
    <h2>Other Projects</h2>
    <div id="project_slider" class="slick-slider">
    <?php

    global $post;
    $args = array( 'posts_per_page' => -1, 'posts_per_page' => 6, 'post_type' => 'portfolio-piece', 'exclude' => get_the_ID() );

    $posts = get_posts( $args ); $x = 1;
    foreach ( $posts as $post ) : setup_postdata( $post ); ?>
        <div class="slick-slide">
            <a class="portfolio-item" href="<?php the_permalink(); ?>">
              <div class="item-info">
                <h3><?php the_title(); ?></h3>
                <p><?php the_field('project'); ?></p>
                <span class="icon-arrow-right"></span>
              </div>
              <span class="thumb" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></span>
          </a>
        </div>
    <?php $x++; endforeach; 
    wp_reset_postdata();?>
    </div>
    
    </div>
  </main><!-- #main -->
</div><!-- #primary -->
<script>
(function ($, root, undefined) {
	
  $(function () {

    $('#project_slider').slick({
      slidesToShow: 5,
      responsive: [
        {
          breakpoint: 2560,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            adaptiveHeight: true,
          }
        }
      ]
    });
  });
	
})(jQuery, this);
</script>
<?php get_footer();