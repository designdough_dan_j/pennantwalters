<?php
/**
 *
 *
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

$nav = array(
  'The Walters Way',
  'History',
  'Team',
  'Compliance',
); 

get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <?php
    while ( have_posts() ) : the_post();
    
      global $flex_row;
      $flex_row = 1;
    
      get_template_part( 'flex' );

    endwhile; // End of the loop.
    ?>
    <?php $flex_row = 1; ?>
    <div class="flex-row flex-row-<?php echo $flex_row; ?> flex-hero">
      <div class="wrap">
        <?php sideways_header($flex_row, "About Us"); ?>
        <div class="content">
          <?php foreach($nav as $key => $item): $img = get_field(sanitize_key($item).'_hero'); ?>
          <?php if($img): ?>
          <div id="<?php echo sanitize_key($item).'_hero'; ?>" class="hero-img" style="<?php if($key === 0): ?>display:block;<?php endif; ?>">
            <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" title="<?php echo $img['title']; ?>">
          </div>
          <?php elseif( get_field(sanitize_key($item).'_video') ): ?>
          <div id="<?php echo sanitize_key($item).'_hero'; ?>" class="hero-video" style="<?php if($key === 0): ?>display:block;<?php endif; ?>">
            <div class="video-wrap">
              <div class="replay-overlay"><a class="replay-btn"><i class="icon-replay" aria-hidden="true"></i>Replay</a></div>
              <video id="<?php echo sanitize_key($item).'_video'; ?>" muted <?php if($key === 0): ?>autoplay<?php endif; ?>>
                <source src="<?php echo get_field(sanitize_key($item).'_video'); ?>" type="video/mp4">
              </video>
            </div>
          </div>
          <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <?php $flex_row++; ?>
    <div class="flex-row flex-row-<?php echo $flex_row; ?>">
      <div class="wrap">
        <div class="content clearfix">
          <?php sideways_header($flex_row, "Our History", 1); ?>
          <aside id="content_left">
            <h1><?php the_title(); ?></h1>
            <?php foreach($nav as $key => $item): ?>
            <div id="<?php echo sanitize_key($item); ?>_pull_quote" class="pull-quote">
              <?php the_field(sanitize_key($item).'_pull_quote') ?>
            </div>
            <?php endforeach; ?>
          </aside>
          <article id="content_right">
            <div class="nav nav-tabs count-<?php echo count($nav); ?> no-bullets">
              <hr/>
              <ul class="clearfix">
                <?php foreach($nav as $key => $item): ?>
                <li>
                  <a id="<?php echo sanitize_key($item); ?>_btn" class="<?php if($key === 0): ?>active<?php endif; ?>" href="#<?php echo sanitize_key($item); ?>"><span class="ball"></span><span class="title"><?php echo $item; ?></span></a>
                </li>
                <?php endforeach; ?>
              </ul>
            </div>

            <div class="tab-intros">
              <?php foreach($nav as $key => $item): ?>
              <div id="<?php echo sanitize_key($item); ?>_intro" class="tab-intro" style="<?php if($key === 0): ?>display:block;<?php endif; ?>">
                <h3><?php the_field(sanitize_key($item).'_title'); ?></h3>
                <?php the_field(sanitize_key($item).'_content') ?>
              </div>
              <?php endforeach; ?>
            </div>
          </article>
        </div>
      </div>
    </div>
    <?php $flex_row++; ?>
    <div class="flex-row flex-row-<?php echo $flex_row; ?>">
      <div class="wrap">
        <div class="content">
        <?php sideways_header($flex_row, "Latest News", 1); ?>
          <div class="tab-content-area">
            <div id="<?php echo sanitize_key($nav[0]); ?>" class="tab-content" style="display:block;">
              <div class="content">
                <h2 class="flex-title">Latest News</h2>
                <div id="news_slider" class="slick-slider">
                  <?php
                  
                  $args = array( 'posts_per_page' => 6 );

                  $posts = get_posts( $args );
                  foreach ( $posts as $post ) : setup_postdata( $post ); ?>
                  <div class="slick-slide">
                    <div class="news-item">
                      <div class="tags">
                        <?php the_tags( '', ' | ', '' ); ?>&nbsp;
                      </div>
                      <a class="thumb" href="<?php the_permalink(); ?>" style="background-image:url(<?php the_post_thumbnail_url(); ?>)"></a>
                      <h3><?php the_title(); ?></h3>
                      <?php the_excerpt(); ?>
                      <a href="<?php the_permalink(); ?>" class="hide-text icon-arrow-right" tabindex="0"><span class="screen-reader-text">Read More</span></a>
                    </div>
                  </div>
                  <?php endforeach;
                  wp_reset_postdata();?>
                </div>
              </div>
              
            </div>
            <div id="<?php echo sanitize_key($nav[1]); ?>" class="tab-content">
              <h2>Walters Timeline</h2>
              <div class="timeline clearfix no-bullets">
                <span class="line horizontal"></span>
                <ul>
                  <?php
                  
                  $timeline = get_field('timeline');

                  // check if the repeater field has rows of data
                  if( have_rows('timeline') ): $x = 1;

                      // loop through the rows of data
                      while ( have_rows('timeline') ) : the_row();
                  $img = get_sub_field('timeline_img');
                  ?>
                  <li class="item-<?php echo $x; ?> <?php if($x % 2 == 0) { echo "left "; } else { echo "right "; } if($x > (count($timeline) - 2)){ echo "no-margin "; }  if($img) { echo "with-img "; } ?>">
                    <div class="timeline-item clearfix">
                      <div class="timeline-desc">
                        <h4 class="timeline-year"><?php the_sub_field('timeline_year'); ?></h4>
                        <?php the_sub_field('timeline_content'); ?>
                        <span class="more" data-target="#timeline_full_<?php echo $x; ?>"><i class="plus-sign" aria-hidden="false"></i>Read More</span>
                        <?php if( $img ): ?>
                        <div class="thumb" style="background-image:url(<?php echo $img['url']; ?>);"></div>
                        <?php endif; ?>
                      </div>
                      <span class="line vertical"><span class="ball"></span></span>
                    </div>
                  </li>
                  <div id="timeline_full_<?php echo $x; ?>" class="timeline-content-full">
                    <a class="close" href="#timeline_full_<?php echo $x; ?>">&#10006;</a>
                    <h4 class="timeline-year"><?php the_sub_field('timeline_year'); ?></h4>
                    <?php the_sub_field('timeline_content'); ?>
                    <?php the_sub_field('timeline_content_more'); ?>
                  </div>
                  <?php
                      $x++;

                      endwhile;

                  endif;

                  ?>
                </ul>
              </div>
            </div>
            <div id="<?php echo sanitize_key($nav[2]); ?>" class="tab-content">
              <div class="grid team clearfix">
                <?php

                // check if the repeater field has rows of data
                if( have_rows('team') ): $x = 1;

                  // loop through the rows of data
                  while ( have_rows('team') ) : the_row(); $img = get_sub_field('img');
                
                if( $img ){
                  $photo = $img['url'];
                } else {
                  $photo = get_template_directory_uri().'/img/person-placeholder-'.get_sub_field('placeholder').'.jpg';
                }
                
                ?>
                <div class="col col-<?php echo $x; ?>" data-category="<?php echo sanitize_title(get_sub_field('category')) ?>">
                  <div class="team-member-wrap">
                    <img src="<?php echo $photo; ?>" alt="<?php the_sub_field('name'); ?> - <?php the_sub_field('role'); ?>" />
                    <div class="team-member-bio">
                      <?php if(get_sub_field('email')): ?>
                      <a class="icon-email" href="mailto:<?php the_sub_field('email'); ?>"></a>
                      <?php endif; ?>
                      <h3 class="team-member-name"><?php the_sub_field('name'); ?></h3>
                      <p class="team-member-role"><?php the_sub_field('role'); ?></p>
                    </div>
                  </div>
                </div>
                <?php
                  $x++;
                  endwhile;

                endif;

                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </main><!-- #main -->
</div><!-- #primary -->
<script>
(function ($, root, undefined) {
	
  $(function () {

    $('#news_slider').slick({
      slidesToShow: 3,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            adaptiveHeight: true,
          }
        }
      ]
    });
    
    <?php

      // check if the repeater field has rows of data
      if( have_rows('timeline') ): $x = 1;

          // loop through the rows of data
          while ( have_rows('timeline') ) : the_row();
      ?>
    
    
      var tween_<?php echo $x; ?> = new TimelineMax ()
      .add([
        TweenMax.from(".item-<?php echo $x; ?> .line", 1, {width: 0, ease: Linear.easeNone}),
        TweenMax.from(".right.item-<?php echo $x; ?> .timeline-desc", 1, {marginLeft: 0, ease: Linear.easeNone}),
        TweenMax.from(".left.item-<?php echo $x; ?> .timeline-desc", 1, {marginRight: 0, ease: Linear.easeNone}),
        //TweenMax.from(".right.item-<?php echo $x; ?> .thumb", 1, {right: 165, ease: Linear.easeNone}),
        //TweenMax.from(".left.item-<?php echo $x; ?> .thumb", 1, {left: 165, ease: Linear.easeNone}),
      ]);

      // build scene
      var scene_<?php echo $x; ?> = new ScrollMagic.Scene({triggerElement: ".item-<?php echo $x; ?>", triggerHook: 'onEnter', duration: 500})
      .setTween(tween_<?php echo $x; ?>)
      .addTo(controller);
    
      <?php
          $x++;

          endwhile;

      endif;

      ?>
      
    });
  })(jQuery, this);
</script>
<?php get_footer();