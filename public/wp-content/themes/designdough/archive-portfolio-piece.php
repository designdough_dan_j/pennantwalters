<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

global $post;

get_header(); ?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

  <?php $rowIndex = (get_row_index() - 1);
  $the_query = new WP_Query(array('post_type'=>'portfolio-piece', 'post_status'=>'publish', 'posts_per_page' => -1)); ?>

    <div class="content">
      <div id="flex_row_1" class="flex-row-1 flex-row">
        <div class="wrap">
          <?php sideways_header(1, 'Project'); ?>
          <div class="clearfix">
            <div class="select-filter multiple">
              <div class="form-group">
                <label>Filter: </label>
                <div class="select icon-caret">
                <select id="projectcategoryfilter" class="w-full" name="projectcategoryfilter">
                    <option selected value="">All Categories</option>
                    <?php foreach( get_terms('portfolio-category') as $category ): ?>
                    <option value="<?php echo $category->name; ?>"><?php echo $category->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <!-- posts_filter -->
                </div>
                <!-- .select -->
                <div class="select icon-caret">
                <select id="projecttagfilter" class="w-full" name="projecttagfilter">
                    <option selected value="">All Tags</option>
                    <?php foreach( get_terms('portfolio-tag') as $tag ): ?>
                      <option value="<?php echo $tag->name; ?>"><?php echo $tag->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <!-- posts_filter -->
                </div>
                <!-- .select -->
              </div>
              <!-- .form-group -->
            </div>
            <!-- .select-filter -->
            <h1 class="page-title">Project</h1>
          </div>
          <div id="response-3">
          <div class="grid news-grid">
              <?php while ($the_query->have_posts()) {?>
              <?php $the_query->the_post();?>
              <div class="col">
                  <a class="portfolio-item" href="<?php the_permalink(); ?>">
                    <div class="item-info">
                      <h3><?php the_title(); ?></h3>
                      <p><?php the_field('project'); ?></p>
                      <span class="icon-arrow-right"></span>
                    </div>
                    <span class="thumb" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></span>
                  </a>
              </div>
              <?php wp_reset_postdata();
             } ?>
          </div>
          </div>
        </div>
      </div>
    </div>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer();
