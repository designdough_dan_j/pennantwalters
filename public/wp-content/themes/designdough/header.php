<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

global $social;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#083050">
    <meta name="theme-color" content="#f1f0ef">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php wp_head(); ?>
      <script src='https://www.google.com/recaptcha/api.js'></script>

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140965193-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-140965193-1');
      </script>
  </head>
  <body <?php body_class(); ?>>
    <!-- Site Wrap -->
    <div class="site-wrap">
      <script>
          var controller;
        (function ($, root, undefined) {

          $(function(){
            // init controller
            controller = new ScrollMagic.Controller();
          });
        })(jQuery, this);
      </script>
      <div class="site-loader"><div class="loader"></div></div>
      <div id="page" class="site">
        <div class="site-overlay"></div>
        <header id="masthead">
          <div class="nav-wrapper clearfix">
            <div class="wrap">
              <a id="nav-toggle" class="nav-toggle">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              </a>
              <nav id="mainnav" class="no-bullets">
                  <?php wp_nav_menu( array( 'menu_class' => null, 'menu_class' => '', 'theme_location' => 'header-menu', ) ); ?>
              </nav>
              <a class="nav-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"></a>
              <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" aria-hidden="true"><?php echo bloginfo('sitename'); ?></a>
              <div class="social-links no-bullets">
                <ul class="clearfix">
                  <?php foreach( $social as $key ): ?>
                  <li><a title="<?php echo $key; ?>" class="hide-text icon-<?php echo sanitize_title($key); ?>" href="<?php the_field(sanitize_title($key), 39); ?>"><?php echo $key; ?></a></li>
                  <?php endforeach; ?>
                </ul>
              </div>
            </div>
          </div>
          <div class="wrap">
            <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo bloginfo('sitename'); ?></a>

            <?php $offices = get_field('offices', 39); ?>
            <div class="office">
              <h4><?php echo $offices[0]['office_name']; ?></h4>
              <p><a href="tel:+44:<?php echo $offices[0]['office_tel']; ?>"><span class="highlight">T</span> &nbsp; 0<?php echo $offices[0]['office_tel']; ?></a></p>
              <p><a href="mailto:<?php echo $offices[0]['office_email']; ?>"><span class="highlight">E</span> &nbsp; <?php echo $offices[0]['office_email']; ?></a></p>
            </div>

          </div>
        </header>
