<?php
        //your site secret key
        //$secret = '6LfwuXsUAAAAAP4UzmyexIPl9T4tHH0lKPSg8JEu';
        $secret = '6LfOlMcUAAAAADihD8T4ur6sPrNuhGD6mZMr_NEh';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);

        if ($responseData->success) {
            //contact form submission code
            error_reporting(E_ALL);
            ini_set('display_errors', 1);

            $parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
            require_once($parse_uri[0] . 'wp-load.php');

// assumes $to, $subject, $message have already been defined earlier...

            $th = 'text-align: left; color: #082f4f; padding: 5px 10px; font-family: Georgia, Times, serif; font-weight: normal; border-right: 1px solid #e9eaed';
            $td = 'text-align: left; padding: 5px 10px;';


            $fields = array(
                'Full Name' => $_POST['full_name'],
                'Email Address' => $_POST['email_address'],
                'Contact Number' => $_POST['contact_no'],
                'Employment Status' => $_POST['emp_status'],
                'Start Date' => $_POST['date_day'] . '-' . $_POST['date_month'] . '-' . $_POST['date_year'],
            );

            $message =
                '<html>
    <body>
      <table style="border-collapse: collapse;font-family: Open Sans, sans-serif; ">';

            foreach ($fields as $key => $val) {
                $message .=
                    '<tr>
      <th style="' . $th . '">' . $key . '</td>
      <td style="' . $td . '">' . $val . '</td>
      </tr>';
            }

            $message .= '<tr>';
            $message .= '<td colspan="2" style="' . $td . '"><hr style="border: none; height: 1px; background: #e9eaed;" /></td>';
            $message .= '</tr>';

            $message .= '<tr>';
            $message .= '<td colspan="2" style="' . $td . '">' . $_POST['enquiry'] . '</td>';
            $message .= '</tr>';

            $message .=
                '</table>
    </body>
  </html>';

            if ( ! function_exists( 'wp_handle_upload' ) ) {
                require_once( ABSPATH . 'wp-admin/includes/file.php' );
            }

            $uploadedfile       = $_FILES['cv-upload'];
            $movefile           = wp_handle_upload( $uploadedfile, array('test_form' => FALSE));

            $to = 'TerriPayne@walters-group.co.uk';
//            $to = 'gareth@designdough.co.uk';
            $subject = get_bloginfo('name') . ' Website Enquiry';
            $headers = array(
                'Content-Type: text/html; charset=UTF-8',
                'From: ' . get_bloginfo('name') . ' <wordpress@walters-group.co.uk>',
                'Reply-To: ' . $_POST['full_name'] . '<' . $_POST['email_address'] . '>'
            );

            if ($movefile) {
                $attachments = $movefile[ 'file' ];
                $mail = wp_mail($to, $subject, $message, $headers, $attachments);

                if ($mail) {
                    $thanksMail = wp_mail($fields['Email Address'], 'Walters Application', 'Thank you for your application, we will be in touch in due course.', 'From: Walters Careers <careers@walters-group.co.uk>');
                }
            }
            else {
                echo "Possible file upload attack!\n";
            }


            if ($mail) {
                echo 'Thanks ' . $_POST['full_name'] . '. Your message has been sent';
                header('Location: http://walters-group.co.uk/walters-careers/');
            } else {
                echo 'Error sending message';
            }

        } else {
            $errMsg = 'Robot verification failed, please try again.';
        };

