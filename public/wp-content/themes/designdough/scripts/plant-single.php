<?php

header('Content-Type: application/json');

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$args = array(
	'posts_per_page' => -1,
	'post_type' => 'plant'
);

$post = get_post(htmlspecialchars($_GET['post_id']));

$cats = array();

$customFields = get_fields();

foreach ( get_the_terms($post->ID, 'plant-category') as $cat )
{
	$cats[] = array(
		'name' => $cat->name,
		'count' => ($cat->count < 10) ? '0' . $cat->count : $cat->count
	);
}

$post_array[] = array(
	'post_id' => $post->ID,
	'post_title' => $post->post_title,
	'post_content' => $post->post_content,
	'link' =>  get_the_permalink( $post->ID ),
	'img' => get_the_post_thumbnail_url( $post->ID ),
	'cat' => $cats[0],
	'fields' => $customFields,
	'image_count' => count($customFields['images'])
);


print json_encode($post_array, true);
