<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage designdough
 * @since 1.0
 * @version 1.0
 */

get_header();
$user = get_queried_object();
$role = $user->roles[0];
?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <div class="wrap">
      <h1 class="flex-title">
        <?
        
        if( $role === "client" ){
          echo $user->display_name;
        } else {
          echo $user->first_name . ' ' . $user->last_name;
        }
        
        ?>
      </h1>
      
      <aside>
        <h2>Contact Details</h2>
        <table class="contact-details">
          <tr>
            <th>Name</th>
            <td><?php echo $user->first_name . ' ' . $user->last_name; ?></td>
          </tr>
          <tr>
            <th>Email</th>
            <td><a href="mailto:<?php echo $user->user_email; ?>"><?php echo $user->user_email; ?></a></td>
          </tr>
          <tr>
            <th>Website</th>
            <td><a target="_blank" href="<?php echo $user->user_url; ?>"><?php echo $user->user_url; ?></a></td>
          </tr>
          <tr>
            <th>Telephone</th>
            <td><?php the_field('client_tel', 'user_'.$user->ID); ?></td>
          </tr>
          <tr>
            <th valign="top">Address</th>
            <td valign="top"><?php the_field('client_address', 'user_'.$user->ID); ?></td>
          </tr>
        </table>
        <?php

        // check if the repeater field has rows of data
        if( have_rows('client_social', 'user_'.$user->ID) ):
        ?>
        
        <div class="client-social">
          <?php

            // loop through the rows of data
            while ( have_rows('client_social', 'user_'.$user->ID) ) : the_row();
        ?>
          <a class="hide-text icon-<?php echo sanitize_title( get_sub_field('social_name') ); ?>" target="_blank" href="<?php the_sub_field('social_url'); ?>"><?php the_sub_field('social_name'); ?></a>
        <?php

            endwhile;
          ?>
        </div>
          <?php

        endif;

        ?>
      </aside>
      <article>
        <div class="web-process">
          <?php

          $field = get_field_object('website_status');
          $web_status = get_field('website_status');

          if( $field ): ?>
          <h2><?php echo $field['label']; ?></h2>
          <?php foreach( $field['choices'] as $key => $value ):?><div class="web-status-wrap w<?php echo $key; ?> <?php if( $web_status['value'] == $key ): ?>current<?php endif; ?><?php if( $web_status['value'] > $key ): ?>prev<?php endif; ?>">
            <div class="web-status-bubble">
              <span class="inner">W<?php echo $key; ?></span>
            </div>
            <span class="web-status-label"><?php echo $value ?></span>
          <span class="line"></span>
          <span class="line line-1"></span>
          <span class="line line-2"></span>
          <span class="line line-3"></span>
          </div><?php
            endforeach;
          endif;
          ?>
        </div>
        
        <h2><?php if( $role != "client" ): ?>Assigned<?php endif; ?> Tickets</h2>
      
        <table class="support-tickets">
          <thead>
            <tr>
              <th class="subject">Subject</th>
              <th class="client">Client</th>
              <th class="assignee">Assignee</th>
              <th class="date">Date</th>
              <th class="status">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php
            
            global $wpdb;
            
            if( $role === "client" ){
            
              $tickets = $wpdb->get_results( 'SELECT id, ticket_type, ticket_summary, ticket_status, client_id, assignee_id, date_added FROM '.$wpdb->prefix.'tickets WHERE client_id = ' . $user->ID );
            
            } else {
              
              $tickets = $wpdb->get_results( 'SELECT id, ticket_type, ticket_summary, ticket_status, client_id, assignee_id, date_added FROM '.$wpdb->prefix.'tickets WHERE assignee_id = ' . $user->ID );
              
            }
            
            foreach($tickets as $ticket):
            
            get_template_part('template-parts/support/table-row');
            
            endforeach; ?>
          </tbody>
        </table>
      </article>
    </div>
  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer();