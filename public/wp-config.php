<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
if ($_SERVER['HTTP_HOST'] == 'pennantwalters.test') {
    define('DB_NAME', 'pennant_walters');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'Pablo001');
} else {
    define('DB_NAME', 'walters');
    define('DB_USER', 'walters');
    define('DB_PASSWORD', '7lKbq70&');
}

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fu78X}Aaj@ESqZw>BQEg+!)Mc;]Vn$=!WeI@4NC;c%zjva^(I%E@Ck94?9UXa98T');
define('SECURE_AUTH_KEY',  'ouDl<ZW:Zef0@l>aQgC;o~`>I=8%JG]j+y-z{6,m9{`mRl;R~?3kx`!^@ Ut4KkX');
define('LOGGED_IN_KEY',    '9|WkXb/j33Q:jI m|TqKXK*V!(-N[cTZazS$LlrQsIWUZSwGQP4=J6Vc=7x+=RK1');
define('NONCE_KEY',        ']%gvKzj* _.#HeCf6MM{M.f6O`o)Qx]`;9PE!/W&Xr56js`_.lj>~yP2QA! :ucz');
define('AUTH_SALT',        'D9%2Y:mJZI@00e{LMpR)-Hty _%uREr9rihpNtgY6yHe_U&h,j[otb;f?Ae%xQRI');
define('SECURE_AUTH_SALT', 'OK]|g?R+0j_7(M5}R%.A9@vSB,] kcQNkE}9@3ERY[0Tl6m?om,A#@w2v-5>441O');
define('LOGGED_IN_SALT',   'rSGm/[qHacbADrCPTz^%Xw &E,&j?I!<8%l[*[X}$[Lf(Jycfq=6n0&0baEW%a|;');
define('NONCE_SALT',       ')a6h/oEM6o5i,n7A<FcRN ~mZCL=IKMKv^7f{[$Do,MKxT/P]6Xzk/NEB~MMSsB<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
